.. Elaine Tissue Stimulation documentation master file, created by
   sphinx-quickstart on Fri May  6 15:54:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Elaine's Tissue Stimulation's documentation!
=======================================================
Elaine's Tissue Stimulation (ETS) provides a pipeline to compute the electrical
field in the biological tissues under stimulation. The quasi-electro static (EQS)
formulation of Maxwell's equations are solved for stimulations with a repetition
rate of up to kHz range (TODO: verify the validity range of EQS and link to the 
paper).

Broadly, ETS pipeline can be decomposed to three different steps each relying on
one or more other open-source package (highlighted in parenthesis):

- **Pre-processing**: analyzing the biological images (`nibabel <https://nipy.org/nibabel/>`_), generating the gometry (`ngsolve <https://docu.ngsolve.org/latest/>`_), and meshing (`ngsolve <https://docu.ngsolve.org/latest/>`_), 
- **Processing**: calculation of the field in the tissue (`ngsolve <https://docu.ngsolve.org/latest/>`_),
- **Post-processing**: to compute some derivative quantities of interest such as activation rate of axons (`neuron <https://nrn.readthedocs.io/en/latest/index.html>`_ and `brian <https://brian2.readthedocs.io/en/stable/>`_), volume of tissue activated (`neuron <https://nrn.readthedocs.io/en/latest/index.html>`_ and `brian <https://brian2.readthedocs.io/en/stable/>`_), visualization (`paraview <https://www.paraview.org/>`_)


.. graphviz::
    :name: sphinx.ext.graphviz
    :caption: ETS pipeline flow; Colors signify separate "inputs", "pre-", "post-", and the main "processing" steps 
    :align: center
    
    
    digraph foo {
      "Signal parameters" -> "Signal" [ label = "0" ];
      "Implantation parameters" -> "Region of Interest" [ label = "1"];
      "Electrode parameters" -> "Electrode profile" [ label = "2" ];
      "MRI" -> "Segmentation mask" [ label = "3" ];
      "DTI" -> "Anisotropy" [ label = "4" ];
      
      "Region of Interest" -> "Geometry" [ label = "5" ];
      "Segmentation mask" -> "Geometry" [ label = "5" ];
      "Electrode profile" -> "Geometry" [ label = "5"];

      "Signal" -> "Fourier decomposition" [ label = "6" ];
      "Fourier decomposition" -> "Conductivities" [ label = "7" ];

      "Geometry" -> "Mesh" [ label = "8" ];
      
      "Conductivities" -> "Weak form" [ label = "9" ];
      "Anisotropy" -> "Weak form" [ label = "9" ];
      "Mesh" -> "Weak form" [ label = "9" ];
      

      "Fourier decomposition" -> "Boundary conditions" [ label = "10" ];
      "Interface parameters" -> "Boundary conditions" [ label = "10" ];
      "Mesh" -> "Boundary conditions" [ label = "10" ];
      
      "Boundary conditions" -> "Solution (Freq. domain)" [ label = "11" ];
      "Weak form" -> "Solution (Freq. domain)"  [ label = "11" ];

      "Solution (Freq. domain)" -> "Mesh refinement" [ label = "12" ];
      "Solution (Freq. domain)" -> "Solution (Time domain)" [ label = "13" ];

      "Solution (Time domain)" -> "VTA" [ label = "14" ];
      "Solution (Time domain)" -> "Axonal activation" [ label = "15" ];
      
      //this one is commented since it makes the graph ugly
      //Mesh refinement" -> "Mesh" [ label = "13" ];
      
      //inputs!
      "MRI" [color="#004d40"];
      "DTI" [color="#004d40"];
      "Electrode parameters" [color="#004d40"];
      "Implantation parameters" [color="#004d40"];
      "Signal parameters" [color="#004d40"];
      "Interface parameters" [color="#004d40"];
      
      //preprocessing steps!
      "Signal" [color="#FFC107"];
      "Segmentation mask" [color="#FFC107"];
      "Geometry" [color="#FFC107"];
      "Electrode profile" [color="#FFC107"];
      "Region of Interest" [color="#FFC107"];
      "Fourier decomposition" [color="#FFC107"];
      "Anisotropy" [color="#FFC107"];
      "Conductivities" [color="#FFC107"];
      "Mesh" [color="#FFC107"];
      
      //processing steps!
      "Mesh refinement" [color="#1E88E5"];
      "Boundary conditions" [color="#1E88E5"];
      "Weak form" [color="#1E88E5"];
      "Solution (Freq. domain)" [color="#1E88E5"];
      "Solution (Time domain)" [color="#1E88E5"];
      
      //post-processing steps!
      "VTA" [color="#D81B60"];
      "Axonal activation" [color="#D81B60"];
    }


.. note::
   This classification is somewhat loose. For meshing, for features such as mesh refinement (which can be seen as a preprocessing step, we do compute the field to figure out which elements must be refined. So take this categories with some salt and pepper.
   

   
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
