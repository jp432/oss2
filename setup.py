# This file is prototypical and doesn't work now.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = '0.1'
setup(
    name='elaine-ts',
    version=version,
    author='Arash Golmohammadi, Jan Philipp Payonk',
    author_email='arash.golmohammadi@uni-rostock.de, jan.payonk@uni-rostock.de',
    packages=['ets', 'ets.utils', 'ets.fem', 'ets.image','ets.signals',
              'ets.geometry', 'ets.visualization'],
    description='ELAINE Tissue Stimulator is a package that provides a pipeline for solving the  electromagnetic stimulation in biological tissues.',
    install_requires=['pyyaml==6.0', 'matplotlib>=3.5'],
    long_description=open('README.rst').read()
)
