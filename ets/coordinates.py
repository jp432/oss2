# -*- coding: utf-8 -*-


import math
import numpy as np
import pdb

import netgen.occ as occ


class Frame:
    
    '''
    This class represents a coordinate system that defines space relative to the orientation of the electrode.  This frame, the "electrode-based frame," can be a useful alternative to the MRI-based "reference frame" that's otherwise in use.

    The basis vectors of this electrode-centered frame are defined as follows:

    * Each basis vector has a length of one unit in the refernce frame.

    * The k basis vector of the electrode-centered frame originates at the tip of the electrode and points in the direction of the electrode's tail.

    * The i basis vector of the electrode-centered frame is parallel to the reference frame's xy plane, perpendicular to the k basis vector, and has an x component with a sign opposite the sign of the k basis vector's y component *unless the k basis vector's y component is zero*.  If the k basis vector's y component is zero, the x componenent of the i basis vector is specified using two different sub-cases:

        #. If the the k basis vector's x component is also zero, the i basis vector is arbitrarily defined as the i basis vector of the reference coordinate system (note how this is the case where the electrode's central lengthwise axis lies entirely along the reference frame's k basis vector).

        #. If the k basis vector's x component is not also zero, the i basis vector's y component will share the same sign as the k basis vector's x component.

    * The j basis vector is defined as k x i


    The figures below should help clarify.  The blue arrows represent the electrode-centered i basis vector, the orange arrows represent the j basis vector, and the red arrows represent the k basis vector.  The thick, grey arrows represent the axes of the reference frame.

    .. figure:: images/i_vector_down.png

    The figure above depicts the direction of electrode-centered i basis vectors relative to vertical electrodes whose tips point towards the xy-plane.  This diagram takes the perspective of looking **straight down on** the xy-plane of the reference frame.

    .. figure:: images/i_vector_up.png

    The figure above depicts the same as the previous figure, except from the perspective of looking **straight up at** the x-y plane of the reference frame.

    .. figure:: images/tilted_electrode.png

    The figure above demonstrates how the i basis vector remains parallel to the xy-plane of the reference frame regardless of the orientation of the electrode.

    '''
    
    def __init__(self, p_tip, p_tail, mark_ang = 0):
        '''
        :param p_tip: this is the location of the tip of the electrode in terms of the refernce frame's coordinates.
        :type p_tip: 1D tuple, 3 elements
        :param p_tail: this is any point along the central lengthwise axis of the electrode in the direction of the electrode's tail.  ``p_tail`` cannot be the same point as ``p_tip`` because such a specification would be ambiguous as to the electrode's orientation.
        :type p_tail: 1D tuple, 3 elements
        :param mark_ang: this is the angle in degrees that the electrode should be rotated around its central lengthwise axis. The exact specifications of this parameter are still not nailed down and will need to be updated before final implementation.
        :type mark_ang: float
        '''
        if (p_tip[0] == p_tail[0]) and (p_tip[1] == p_tail[1]) and (p_tip[2] == p_tail[2]):
            raise Exception("Please enter p_tip and p_tail values that represent different points in reference coordinate space.")
        
        self._ROUND_TO = 6
        
        self._p_tip = p_tip
        self._mark_ang = mark_ang
        
        # The k basis vector starts at p_tip and goes in the direction of p_tail
        dir_vec = [p_tail[0] - p_tip[0], p_tail[1] - p_tip[1], p_tail[2] - p_tip[2]]
        # Use distance formula to obtain the distance between p_tip and p_tail
        dir_vec_mag = math.sqrt( dir_vec[0] ** 2 + dir_vec[1] ** 2 + dir_vec[2] ** 2)
        # If there's a non-zero distance between p_tip and p_tail, scale dir_vec into a unit vector by
        # dividing its components by the distance
        if dir_vec_mag != 0:
            self._k = np.array([dir_vec[0]/dir_vec_mag, dir_vec[1]/dir_vec_mag, dir_vec[2]/dir_vec_mag])
        # If the distance between p_tip and p_tail is 0, k has no magnitude
        else:
            self._k = np.array([0, 0, 0])            

        # The unit vector i starts at p_tip, runs parallel to the reference xy-plane, and, looking down the electrode
        # from its tail to its tip, will point to the right (assuming you are viewing the reference plane with your
        # head at a higher z-coordinate than your feet)
        # Used these equations to solve for i's x and y components:
        #     1. sqrt(ix ** 2 + iy ** 2 + iz ** 2) = 1 (this is because
        #            the magnitude of i, as a unit basis vector, is one)
        #     2. dot_product(i, k) = 0 (because i and k are perpendicular basis vectors)
        #     3. iz = 0 (because the i basis vector is parallel to the xy-plane)
        # Interpreter wasn't throwing ZeroDivisionErrors so I implemented error handling with if/else instead
        # If the electrode's central lengthwise axis lies entirely within the yz-plane, ...
        if self.get_k('x') == 0:
            # ... the y component of the i basis vector will be zero, and ...
            i_y = 0
            # ... if the electrode's central lengthwise axis also lies entirely along the z-axis, ...
            if self.get_k('y') == 0:
                # ... the x component of the i basis vector will be arbitrarily set to equal one, but ...
                i_x = 1
            else:
                # ... if it doesn't lie entirely along the z-axis, the i basis vector will have
                # the opposite sign of the y component of the k basis vector.
                i_x = -1 * np.sign(self.get_k('y'))
        # If the electrode's central lengthwise axis doesn't lie entirely within the yz-plane, ...
        else:
            # ... this is the equation for the y component for its i basis vector (using the equations above), and ...
            i_y = 1/(abs(math.sqrt(1 + ((self.get_k('y')/self.get_k('x')) ** 2))))
            # if the k basis vector points in the negative x direction, ... 
            if self.get_k('x') < 0:
                # ... you'll want to select the negative result from the square root function in the equation above.
                i_y *= -1
            # This is the equation for the x component of the i basis vector as solved for
            # using the three equations above
            i_x = -i_y * self.get_k('y')/self.get_k('x')
        self._i = np.array([i_x, i_y, 0])
        
        # The unit vector j starts at p_tip and is perpendicular to both unit electrode vectors j and k
        self._j = np.cross(self.get_k(), self.get_i())
    
        
        # The following angles are saved for use in the rotate function
        # Theta is the angle between i of the electrode frame an i of the reference frame.
        theta = math.acos(np.array(self.get_i()).dot([1, 0, 0])) * 180/np.pi
        if self.get_i('y') < 0:
            theta *= -1
        self._theta = theta
        # Phi is the angle between k of the electrode frame and k of the reference frame.
        self._phi = math.acos(np.array(self.get_k()).dot([0, 0, 1])) * 180/np.pi
        
        
    
    def to_ref(self, pnt):
        '''
        :param pnt: this is the coordinates of the tip of the electrode in terms of the electrode-centered frame
        
        :type pnt: float tuple
        
        :return: a float tuple representing the coordinates of **pnt** in terms the reference frame
        
        .. note:: ``to_elec`` and ``to_ref`` are prone to floating point rounding errors.        
        '''
        
        if len(pnt) != 3:
            raise Exception("to_ref only converts points with 3 coordinates; the point given has " + len(pnt))
        
        # The transform matrix for the linear transformation portion of
        # converting from the electrode frame to the reference frame
        transform_matrix = np.transpose(np.array([np.array(self.get_i()), np.array(self.get_j()), np.array(self.get_k())]))
        
        # Linearly transform and then translate the given pnt from the electrode frame to the reference frame
        transformed_pnt = np.dot(transform_matrix, np.array(pnt)) + np.array(self.get_p_tip())
        
        # Return the transformed_pnt in the expected format
        formatted = np.transpose(np.round(transformed_pnt, self._get_ROUND_TO()))
        return (formatted[0], formatted[1], formatted[2])
         
    
    def to_elec(self, pnt):
        '''        
        :param pnt: the coordinates of a point in terms of the reference frame
        
        :type pnt: float tuple
        
        :return: a float tuple representing the coordinates of **pnt** in terms the electrode-centered frame
        '''
        if len(pnt) != 3:
            raise Exception("to_elec only converts points with 3 coordinates; the point given has " + len(pnt))
        
        # This is the vector from p_tip to the point given.
        rel_to_elec_orig = np.array(pnt) - np.array(self.get_p_tip())
        
        # The components of the point in electrode coordinates will be the shadow of the components
        # of the above vector on the electrode-centered coordinate system's respective basis vectors
        basis_components = np.array([self.get_i(), self.get_j(), self.get_k()])
        answer = np.round(np.dot(basis_components, rel_to_elec_orig), self._get_ROUND_TO())
        
        return (answer[0], answer[1], answer[2])
    
    
    def rotate(self, geo):
        '''        
        :param geo: this is the geometry of the shape that's oriented in the electrode-centered frame
        
        :type geo: TopoDS_Shape
        
        :return: a TopoDS_Shape identical to **geo** but reoriented in the reference frame rather than in the electrode-centered frame
        
        .. note:: The absolute error of the location of the shape returned by ``rotate`` may be as high as 0.7 units.  This appears to be a result of imprecision on NetGen's part.
        '''
        
        
        geo = geo.Rotate(occ.Axis((0, 0, 0), occ.Z), self._get_theta())
        geo = geo.Rotate(occ.Axis((0, 0, 0), self.get_i()), self._get_phi())
        geo = geo.Rotate(occ.Axis((0, 0, 0), self.get_k()), self._get_mark_ang())
        geo = geo.Move((self.get_p_tip('x'), self.get_p_tip('y'), self.get_p_tip('z')))
        return geo
                             
        
    # i, j, k, and p_tip are stored in tuple form for smoother compatability with NGSolve's OCC Geometry TopoDS_Shape constructors
    
    def get_i(self, a = None):
        '''
        :param a: optional parameter used to specify which component of the i basis vector to return if only a specific component of the vector is desired.  Accepted values are 'x', 'y', 'z', or None.
        :type a: char
        
        :return: information about the i basis vector of the electrode-centered frame in terms of the coordinates of the reference frame.  Without specifying any parameters, ``get_i`` returns the i basis vector of the electrode-centered frame as a 1D tuple of 3 floats.  Passing in 'x', 'y', or 'z' as a value for the parameter **a** will return the respective component of the i basis vector as a float.
        '''
        if a == None:
            return (float(self._i[0]), float(self._i[1]), float(self._i[2]))
        elif a == 'x':
            return float(self._i[0])
        elif a == 'y':
            return float(self._i[1])
        elif a == 'z':
            return float(self._i[2])
        else:
            raise Exception("Please select one of the following options for parameter a:\nblank for the entire i vector\n'x' for its x component\n'y' for its y component\n'z' for its z component")
    
    
    def get_j(self, a = None):
        '''
        :param a: optional parameter used to specify which component of the j basis vector to return if only a specific component of the vector is desired.  Accepted values are 'x', 'y', 'z', or None.
        :type a: char
        
        :return: information about the j basis vector of the electrode-centered frame in terms of the coordinates of the reference frame.  Without specifying any parameters, ``get_j`` returns the j basis vector of the electrode-centered frame as a 1D tuple of 3 floats.  Passing in 'x', 'y', or 'z' as a value for the parameter **a** will return the respective component of the j basis vector as a float.
        :type a: char
        '''
        if a == None:
            return (float(self._j[0]), float(self._j[1]), float(self._j[2]))
        elif a == 'x':
            return float(self._j[0])
        elif a == 'y':
            return float(self._j[1])
        elif a == 'z':
            return float(self._j[2])
        else:
            raise Exception("Please select one of the following options for parameter a:\nblank for the entire j vector\n'x' for its x component\n'y' for its y component\n'z' for its z component")
    
    
    def get_k(self, a = None):
        '''
        :param a: optional parameter used to specify which component of the k basis vector to return if only a specific component of the vector is desired.  Accepted values are 'x', 'y', 'z', or None.
        :type a: char
        
        :return: information about the k basis vector of the electrode-centered frame in terms of the coordinates of the reference frame.  Without specifying any parameters, ``get_k`` returns the k basis vector of the electrode-centered frame as a 1D tuple of 3 floats.  Passing in 'x', 'y', or 'z' as a value for the parameter **a** will return the respective component of the k basis vector as a float.
        '''
        if a == None:
            return (float(self._k[0]), float(self._k[1]), float(self._k[2]))
        elif a == 'x':
            return float(self._k[0])
        elif a == 'y':
            return float(self._k[1])
        elif a == 'z':
            return float(self._k[2])
        else:
            raise Exception("Please select one of the following options for parameter a:\nblank for the entire k vector\n'x' for its x component\n'y' for its y component\n'z' for its z component")

            
    def get_p_tip(self, a = None):
        '''
        :param a: optional parameter used to specify which component of the tip of the electrode to return if only a specific component of the point is desired.  Accepted values are 'x', 'y', 'z', or None.
        :type a: char
        
        :return: information about the tip of the electrode around which the electrode-centered frame is oriented.  This information is in terms of the coordinates of the reference frame.  Without specifying any parameters, this function returns a 1D tuple of 3 floats representing the tip of the electrode around which the the electrode-centered frame is oriented.  Passing in 'x', 'y', or 'z' as a value for the parameter **a** will return the respective component of the electrode tip as a float.
        '''
        if a == None:
            return (float(self._p_tip[0]), float(self._p_tip[1]), float(self._p_tip[2]))
        elif a == 'x':
            return float(self._p_tip[0])
        elif a == 'y':
            return float(self._p_tip[1])
        elif a == 'z':
            return float(self._p_tip[2])
        else:
            raise Exception("Please select one of the following options for parameter a:\nblank for the p_tip coordinates as a tuple of three integers\n'x' for its x component\n'y' for its y component\n'z' for its z component")
    

    def _get_phi(self):
        return self._phi
                             
    def _get_theta(self):
        return self._theta
    
    def _get_mark_ang(self):
        return self._mark_ang
    
    def _get_ROUND_TO(self):
        return self._ROUND_TO
    
