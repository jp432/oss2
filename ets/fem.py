# -*- coding: utf-8 -*-

from mpi4py import MPI
from ngsolve import *
import netgen.occ as occ
import numpy as np
import sys

class FFEM(object):
    """This class solves the finite element problem in parallel. There are two
    parallelization stages (i.e., hybrid programming):

    1. Fourier decomposition of the dispersive solution into independent 
    linear problems evaluated at each frequency (performed on different CPUs) 
    2. Mesh decomposition of every problem through multithreading on one CPU (optional).

    **NOTE**: For the moment I make a cube made of two materials to clarify the interface. But the scripts must be substituted with the correct versions.

    :parameter ``sim``: simulation object 
    """

    def __init__(self, sim, ):
        self.sim = sim
        self.Ncpu = None
        self.geo = self.make_geo()
        self.mesh = self.make_mesh() 
        self.solve_fem_parallel()
        self.impedance_spectroscopy()
        self.inverse_fourier()

    def make_geo(self):
        """"
        Makes the geometry of the region of interest (ROI) in the brain with the electrode subtracted -- thus, only contains the brain tissue. The maximum sizes and boundary names are also defined on the geometry.

        """
        # box1 = occ.Box( Pnt(0,0,0), Pnt(1,1,0.5) )
        # box2 = occ.Box( Pnt(0,0,0.5), Pnt(1,1,1) )

        
        # box1.mat('1')
        # box2.mat('2')

        # box1.faces.Min(occ.X).bc('left')
        # box2.faces.Min(occ.X).bc('left')

        # box1.faces.Max(occ.X).bc('right')
        # box2.faces.Max(occ.X).bc('right')

        # geo = occ.Glue([box1,box2])

        # geo.faces.Max(occ.Z).bc('top')
        # geo.faces.Min(occ.Z).bc('bottom')

        # geo = occ.OCCGeometry(geo)
        
        pass

    def make_mesh(self):
        """Meshes the mesh from the geometry and saves it on the disk."""

        # mesh = Mesh(geo.GenerateMesh(maxh=0.25))
        # mesh.Curve(3)

        # mesh.ngmesh.Save(save_dir, "mesh.vol")
        pass
    
    
    def solve_fem_parallel(self, threading=False):
        """
        A wrapper around `mpi_fem.py` that computes the potential field at each frequency with one core.
        """
        
        # TODO: config is a dictionary that provides the necessary arguments to the parallel_fem function of mpi_fem. Everything must be there at self.sim. but one has to set it up nonetheless.
        config = None

        # TODO: It seems that MPI only accepts maximum number of *sockets*-1 as `maxprocs` argument. Most of the time, a socket on a motherboard has more than one cpu. Therefor, this syntax is blind to huge amount of available resources. Please, check how can we specify the maximum number of CPUs and not the sockets. FYI, -1 is reasonable since the current script needs one of those processors. 
        comm = MPI.COMM_SELF.Spawn(
            sys.executable,
            args = ['mpi_fem.py','config'],
            maxprocs=self.Ncpu-1) 
        
    
    def impedance_spectroscopy(self):
        """This function aggregates the impedances of parallel computations by combining all the impedance csv files into one. If a limited number fo frequencies are sampled over the entire spectrum, then it reconstructs the full spectrum by spline interpolation of the sampled impedances.
        """
        # outline:
        
        # freqs = np.arange(self.nfreqs)*130. # base freq.
        # freqs_per_rank = len(freqs)//nproc
        # tfs = np.zeros_like(freqs, dtype=complex)

        # for rank in range(nproc):
        #     tf = np.loadtxt('tfs_{}.csv'.format(str(rank)), dtype=tuple, converters = {0: lambda x: eval(x)})
        #     tfs[rank*freqs_per_rank:(rank+1)*freqs_per_rank]= tf.astype(complex)
            
        # np.savetxt('tfs.csv'.format(str(rank)), tfs)

    def inverse_fourier(self):
        """take the inverse fourier transform of solutions.
        """
        # outline:
        # each NGsolve solution, ultimately is a vector of size Ndof. If we have N frequencies (= length of the stimulation signal), we can construct a numpy array of size (Ndof, N). It is then easy to use scipy routines to take the fourier inversion of such matrix given the frequencies. It yields a matrix of size (Ndof, N) with N timestamps.

        # NOTE: If the number of frequencies are smaller than the length of the signal, the normal fourier inversion doesn't work and we need to resort to something like octave band. Yet, I'm not aware of the details.
        #  
        pass