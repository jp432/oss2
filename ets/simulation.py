# -*- coding: utf-8 -*-

import image
import geometry
import signals
import fem

import os

FINAL_STATE = 15 # the count of all possible simulation states

class Simulation(object):
    """The high-level class for running a simulation. Each instance, internally 
    keeps track of a ``state``, an integer that determines the the location of 
    the simulation on the pipeline graph. Upon instantiation, all the arguments
    are assessed. Look below for more details.
    
    There are two modes to conduct the simulation. In *dry-run*, the user can
    navigate through the ETS flowchart (look at documentation webpage) with 
    arbitrary order (provided that the inputs already set up). In this mode,
    ``state`` will **NOT update**. The other mode is *automatic* using the 
    ``run_sequential`` method of the ``Simulation`` class, which crawls the 
    pipeline tree according to the labels of each edge. In this mode ``state``
    will **update** dynamically throughout the simulation.

    .. note::
        Dry run is useful for rerunning an interrupted simulation, checking the
        output of a particular operation (possibly with dummy data) or using a
        particular method for a simple problem you have elsewhere. e.g., imagine
        you have a DTI image and would like to compute the fractional 
        anisotropy. You can set up a simulation case, point to the directories 
        where your files are placed, and just run the ``compute_fa`` method.


    .. note::
        The order of operations in the sequential mode are according to the 
        load of each operation such that the lighter ones are executed first.
    """

    def __init__(self, args, state=None):
        """Instantiates the simulation object given the argument. All the sub-
        dictionaries will be assessed and parsed to SI units before any run.

        :param args: path to the *config.yml* file (read the documentation for
            the config file for configuration detail)
        :type args: str
        :param state: state indicator, defaults to None
        :type state: int or Nonetype, optional
        """

        self.assess_args(args) # complains if invalid
        self.args = args
        
        self.state = state
        self.setup_state()        

        # The following dict, encodes the workflow of the simulation instance.
        # Each state, will point to a `method` that has to run next. The `msg`
        # will be printed out to the user in the terminal for clarity unless in
        # state 8-11 which are done in parallel, thereby printing is turned off.
        self.pipeline = {
            0: dict(method=self.generate_signal, msg="Generating the stimulation signal."),
            1: dict(method=self.fft_decompose, msg="Decomposing the signal into Fourier basis"),
            2: dict(method=self.compute_anisotropy, msg="Starting anisotropy computation."),
            3: dict(method=self.segment_mri, msg="Starting MRI segmentation."),
            4: dict(method=self.make_geo, msg="Building the geometry."),
            5: dict(method=self.generate_mesh, msg="Generating mesh."),
            6: dict(method=self.refine_mesh, msg="Refining the mesh."),
            7: dict(method=self.solve_fem, msg="Solving the Finite element problem."),
                # 6: dict(method=self.compute_conductivities, msg=""),
                # 7: dict(method=self.setup_weak_form, msg=""),
                # 8: dict(method=self.setup_bc, msg=""),
                # 9: dict(method=self.solve_fem, msg=""),
                # 10: dict(method=self.refine_mesh, msg="Refining mesh."),
            8: dict(method=self.compute_vta, msg="Calculating VTA."),
            9: dict(method=self.compute_pa, msg="Calculating the axonal activation."),
            }

    def assess_args(self, args):
        """
        Assesses the argument before assigning it to the class.

        TODO: add where and which function is breaking and what are the valid 
        arguments. Also think of logging, for info, warning, error...  
        """
        length_units = ['m','cm','mm','um','nm']
        current_units = ['A','mA', 'uA','nA']
        voltage_units = ['V','mV', 'uV']

        # 1. i/o
        if os.path.exists(args.directory.image_dir):
            # TODO: checks if the images exist, but nibabel checks if the 
            # images are readable or not later.
            pass
        else: 
            raise NameError("No image directory.")


        # 2. geometry
        # 2.1. domain
        assert args.geometry.domain.shape in ['box','ellipsoid','cylinder'],\
            "Domain shape not recognized."
        assert len(args.geometry.domain.extend)==3, \
            "The domain extend is not three dimensional!"
        for component in enumerate(args.geometry.domain.extend):
            assert component>=0.0, "Some components of the extent are not positive."
        assert args.geometry.domain.extend_unit in length_units,\
            "The extent unit is not valid. Please choose from the following:"+\
                ' '.join(length_units)

        # 2.2. electrode
        valid_profiles = [] # TODO: decide about the profiles and reading it
        assert args.geometry.electrode in valid_profiles, \
            "{} is not a valid profile name.".format(args.geometry.electrode)
        for component in enumerate(args.electrode.implantation_coords):
            # TODO: think of a good assertion check here
            continue
        for component in enumerate(args.electrode.end_coords):
            # TODO: think of a good assertion check here
            continue
        assert args.domain_geometry.coords_unit in length_units,\
            "Implantation coordinates have an invalid unit. Please choose from the following:"+\
                ' '.join(length_units)
        assert (args.domain_geometry.rotation_angle<=180.0 and \
            args.domain_geometry.rotation_angle>-180.0),\
            "Rotation angle must be between -180 to 180 degrees."
            # TODO: other conventions are also possible. We have to clear it up.
        
        # 2.3 lesion
        assert type(args.geometry.lesion.encapsulation_layer)==bool,\
            "Encapsulation layer must be set either True or False."
        assert (type(args.geometry.lesion.encapsulation_thick)==float and\
            args.geometry.lesion.encapsulation_thick>=0),\
            "Encapsulation layer thickness must be a non-negative float."
        assert args.geometry.lesion.thickness_unit in length_units,\
            "The unit of encapsulation layer thickness is invalid. Please choose from the following:"+\
                ' '.join(length_units)
        assert (type(args.geometry.lesion.fillet_size)==float and\
            args.geometry.lesion.fillet_size>=0),\
            "Fillet size must be a non-negative float."
        if args.geometry.lesion.fillet_size:
            assert args.geometry.lesion.fillet_units in length_units,\
                "The fillet unit is invalid. Please choose from the following:"+\
                ' '.join(length_units)
        
        # 3. physics
        assert args.physics.formulation in ['EQS','ES'],\
            "Formulation is not recognized. Please select from 'EQS' or 'ES'."
        assert args.physics.conductivity_model in ['gabriel','julius'],\
            "Connectivity model not recognized. Please select from 'gabriel' and 'julius'."
        

        # 4. signal


        # 5. stimulation


        # 6. Feature

    def parse_args(self):
        """
        Parses the arguments to convert all the units to SI (meter, amp, volt, 
        radian),
        
        **Suggestion**: after test, can be include assessment as well. 
        """
        pass

    
    def setup_state(self):
        """Based on the internal state, loads the previously calculated results
        or prepares the directories for a new calculation. It will always run
        upon instantiation of the class.
        """

        if self.state==None:
            self.name = 'stim'+'' # TODO: a string made out of datetime
            self.setup_dirs()
        else:
            self.check_dirs() # assess the integrity of directories
            self.load_state() # must read some key that saves the state
        

    def setup_dirs(self):
        if os.path.exists(self.args.directory.result_dir):
            print("The result directory exists. No new folder was created.")
        else:
            os.mkdir(self.args.directory.result_dir)
            print("The result directory created.")
    

    def check_dirs(self):
        """Checks if the folder configuration is correct. In addition, checks
        existence of essential image files.
        """        
        pass

    def load_state(self):
        """Reads the state from a previous state key saved on ``output_dir`` 
        and loads the saved results (if possible)."""
        pass

    
    def run_single_task(self, task):
        """Runs a single task and importantly, increases the simulation state.
        It should NOT be used for dry running of a task (use the class methods
        instead.)

        :param task: name of the function to be run
        :type task: str
        """
        assert task in [s.method.__str__ for s in self.pipeline.values()], \
            "method name {} is not recognized!".format(task)
        eval('self.'+task)()

    def run_sequentially(self):
        """Starts calculating the field.
        """
        if self.state in self.pipeline.keys():
            msg = self.pipeline[self.state]['msg']
            self.pipeline[self.state]['method']()
            self.state += 1
        else:
            raise NotImplementedError("The simulation state is {} which does \
                not correspond to any operation. Either the simulation is  be identified!")
    
    def generate_signal(self):
        """Generates the stimulation signal from the arguments in the 
        *config.yml*.

        TODO: Where to save the signal?
        TODO: What to save? FFT decomposition or the signal?
        """
        t, s = signals.generate_signal(**self.args['signal'])
        self.time = t
        self.signal = s

    def fft_decompose(self):
        """Decomposes the signal into its Fourier basis. 

        #TODO: Where to save?
        """
        fourier, freqs = signals.fft(self.time, self.signal)
        # and other stuff

    def compute_anisotropy(self):
        """Compute the anisotropy tensor if a DTI image exists.
        """
        image.compute_anisotropy(self)

    def segment_mri(self):
        """Segments the MRI images if not segmented.
        """
        image.segment_mri()
    
    def make_geo(self):
        """Generates the region of interest (roi), electrode profile, and 
        (possibly) the encapsulation layer from the arguments in the 
        *config.yml*.
        """
        geo = geometry.generate_domain(**self.args['geometry']['domain'])
        geo = geometry.generate_electrode(**self.args['geometry']['electrode'])
        geo = geometry.add_encapsulation(**self.args['geometry']['lesion'])
        # other stuff

    def generate_mesh(self):
        """Meshes the generated geometry according to a certain strategy.
        
        #TODO: define those strategies.
        """
        mesh = fem.generate_mesh(self)

    def refine_mesh(self):
        """Nothing at this point.
        """
        pass

    def solve_fem(self):
        """Solves the fourier FEM problem in parallel using the settings given
        in *config.yml*. 

        # TODO:
        # 1. build a simple in function that runs in parallel
        # 2. make sure we can pass different args to it
        # 3. substitute it with a simple poisson problem (or even capacitor recharge)
        # 4. think of saving their outputs
        # 5. make sure the solution aggregation works fine
        # 6. substitute the poisson problem with an advanced method in fem module
        """
        pass


    def compute_vta(self):
        pass

    def compute_pa(self):
        pass

    def save(self):
        pass

    def load(self):
        pass