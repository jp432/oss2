import numpy as np
import pandas as pd

# Permittivity of free space:
from scipy.constants import epsilon_0 as e0


def cc(freq, ep_inf, param_array, sigma):
    
    '''
    This function calculates permittivity using the Cole-Cole (CC) equation and the values given in the parameters.  It is
    currently only used in ETS as CC2 and CC4 (2 sum terms and 4 sum terms), but it should be able to work with any number of
    sum terms (sum terms refers to the value above the sigma in sigma notation, n).
    
    :param freq: omega, the frequency at which to calculate the permittivity.  Units for freq are rad/s.
    :type freq: float
    
    :param ep_inf: the epsilon infinity value to use to calculate this permittivity
    :type ep_inf: float
    
    :param param_array: must have length of the number of sum terms (for example, 4CC would use a parameter array of length 4).  Each entry of param_array is a list of three elements.  Each of the three elements is one the values in the respective sum term in the following order: delta epsilon (dep), tau (units in s), and exp (the value that goes in the exponent in the denominator.  Note that when using Gabriel & Gabriels' parameters, the exp parameters are entered as '1 - alpha'.  When used with Zimmermans', the exp parameters are entered as 'a'.  This is due to the difference in the way the equation is written in Gabriels' and Zimmerman's papers.
    :type param_array: list of lists of three floats
    
    :param sigma: the sigma value used to calculate this permittivity.  Units are S m^(-1).
    :type sigma: float
    
    :returns: float
    '''
    
    # This is the Cole-Cole equation
    perm = ep_inf + np.sum(param_array[:,0]/(1 + ((1j * freq * param_array[:,1]) ** param_array[:,2])))

    # A frequency of zero is a special case
    if not np.isclose(freq, 0):
        perm = perm + sigma/(1j * freq * e0)
        
    return perm


def comp_perm_cond(method, tissue_name, freqs, round_decimals = 6):

    '''
    This function computes the permittivities and conductivities for the given frequencies.  Return type is
    numpy arrays with complex values; the first on returned is the permittivities, and the second the conductivities.
    
    :param method: the method to use for calculations
    :type method: string
    
    The currently supported method options are:
    
        * "4ccg" - four-term Cole-Cole equation using the parameters provided by Gabriel & Gabriel
        * "eq5g" - equation five from Gabriel & Gabriel; also uses the parameters provided by Gabriel & Gabriel
        * "2ccz" - two-term Cole-Cole equation using the parameters provided by Gabriel & Gabriel
        * "hnz" - the Havriliak-Negami equation using the parameters provided by Zimmerman
        
    In the future, three- and four-term Cole-Cole equation methods using Zimmerman's parameters could be useful
    to implement.  They have not been added already because it is unclear how the parameters Zimmerman provides for these
    methods apply to the Cole-Cole equation (specifically with the exponent for the denominator term in the for the
    four-term version).
        
    :param tissue_name: the abbreviation of the tissue the permittivities and conductivities should be calculated on
    :type tissue_name: string

    The currently supported tissue abbreviations are:
     
        * "gm" - grey matter
        * "wm" - white matter
    
    :param freqs: the frequencies at which the permittivities and conductivities will be calculated.  The order of the permittivity and conductivity lists returned by this function will correspond to the order of the frequencies in this list. Units for each frequency should be in rad/s.
    :type freqs: float list
    
    :param round_decimals: the number of decimals the returned values should have.  The None keyword can be entered for this parameter to specify no rounding.
    :type round_decimals: int
    
    Moving forward, implementing functionality for significant figures should be considered.  It would probably be easiest to
    implement once everything else has been implemented since values are typically rounded to their signficant figures at the end
    of all the calculations.  
    '''
    
    # This file relies on the csv file that stores the conductivity parameters provided in the
    # Gabriel & Gabriel and Zimmerman papers.  The format of this file is as follows.
    # Each row represents a parameter, which author provided the parameter, and where in their paper it came from.  For example, 
    # g_1_dep1 indicates that the parameters in its row are from Gabriel & Gabriel's (g) table one (1) and that the parameter is 
    # the delta epsilon value to be used in the first term of the summation in the Cole-Cole equation (dep1; as a side note, we 
    # know that this parameter is going to be used for Cole-Cole because Table One of Gabriel & Gabriel's paper provides 
    # parameters for use in the four-term Cole-Cole equation).  Note that the strings at the head of each row could be modified 
    # as long as the changes are reflected in the code.
    # Each column represents a different tissue type.  The entry at the intersection of the "g_1_dep1" row with the "gm" column
    # indicates that the value is the delta epsilon value for greay matter ("gm") the first term of the Cole-Cole summation and
    # that this value came from Table One in the paper by Gabriel & Gabriel.
    
    # Note how runtime efficiency is particularly important for this function since it needs to be called for every
    # frequency for the finite element calculations.  With this in mind, it may be better to hardcode the conductivity values
    # into the code (this file was originally written that way) in order to save the time it takes to read from the csv.
    # However, I think it would generally be considered that this way of doing it is more poorly organized.
    
    
    # No need to check that freqs are all >= 0 before starting because this function will be called by a function internal to ets
    # and will not be passed parameters directly from the user, so the parameters passed into this function
    # should have already been checked for compatability by the time this function is called.
    
    
    # Read the parameter values for the given tissue type into a dictionary.
    # The parameter values are currently stored in the csv file 
    # "conductivity_parameters.csv"  The keys of the dictionary are the row headers and its values are the
    # parameters to which they correspond for the given tissue type
    try:
        tissue_df = pd.read_csv("../ets/conductivity_parameters.csv", usecols = ["value_names", tissue_name])
        tissue_df.set_index("value_names", inplace = True)
        tissue_dict = tissue_df.squeeze().to_dict() 
    # If an exception is raised reading in the parameter values, assume it was due to a file error and report the exception
    except:
        raise Exception("Error reading conductivity parameter file.  Please ensure that the file is in the same directory as comp_cond.py and that the tissue identifier given as the tissue_name parameter for comp_cond is a valid column header in the file (for example, \"value_names\", \"gm\", or \"wm\").  The tissue name you entered was " + tissue_name)   
    
    
    
    # Calculate permittivities
    
    # Initialize the numpy array to the correct size instead of appending values to a list each time a new calculation 
    # is performed because it saves time and memory
    perms = -np.ones(len(freqs), dtype = complex)
    
    
            

    if method == "4ccg":

        # Note how the for loop goes inside of the if/else blocks for each method.  This is more runtime efficient than if there
        # was a single for loop encompassing the if/else statements because then the if/else statements would be repeated
        # unnecessarily for each frequency in freqs.
        for x in range(len(freqs)):
            # Cole-Cole with n = 1 to n = 4 with data from Gabriel Table 1                

            # param_array for Cole-Cole function
            # I wouldn't be surprised if there was a more efficient way to implement this with regular expressions - I will
            # look into that
            param_array = np.array([
                           [tissue_dict["g_1_dep1"], tissue_dict["g_1_tau1"], 1 - tissue_dict["g_1_alph1"]],
                           [tissue_dict["g_1_dep2"], tissue_dict["g_1_tau2"], 1 - tissue_dict["g_1_alph2"]],
                           [tissue_dict["g_1_dep3"], tissue_dict["g_1_tau3"], 1 - tissue_dict["g_1_alph3"]],
                           [tissue_dict["g_1_dep4"], tissue_dict["g_1_tau4"], 1 - tissue_dict["g_1_alph4"]]])

            # Save the permittivity calculated for this function
            perms[x] = cc(freqs[x], tissue_dict["g_1_ep_inf"], param_array, tissue_dict["g_1_sigma"])


    elif method == "eq5g":
        for x in range(len(freqs)):
            # Equation 5 using data from Gabriel Table 2 AND ε∞ FROM GABRIEL TABLE 1
            # (did not see specification for the ε∞ parameter in the Zimmerman paper):

            perm = tissue_dict["g_1_ep_inf"] + (tissue_dict["g_2_eps"] - tissue_dict["g_1_ep_inf"])/(1 + (1j * freqs[x] * tissue_dict["g_2_tau"]) ** (1 - tissue_dict["g_2_alph"]))

            if not np.isclose(freqs[x], 0):
                # This line is written repeatedly among the different method implementations.  It could be
                # functionalized, but I chose not to to improve runtime efficiency
                # I did functionalize all of Cole Cole though because it is more than just an if statement
                perm = perm + tissue_dict["g_2_sigma"]/(1j * freqs[x] * e0)

            perms[x] = perm


    elif method == "2ccz":
        for x in range(len(freqs)):
            
            # Cole-Cole 2 equation using data from Zimmerman Table S4:
            param_array = np.array([
                   [tissue_dict["z_s4_dep1"], tissue_dict["z_s4_tau1"], tissue_dict["z_s4_a1"]],
                   [tissue_dict["z_s4_dep2"], tissue_dict["z_s4_tau2"], tissue_dict["z_s4_a2"]]])
            perms[x] = cc(freqs[x], tissue_dict["z_s4_ep_inf"], param_array, tissue_dict["z_s4_sigma"])


        # No 4ccz option yet because we don't know what Zimmerman's denominator exponents are
        '''
        # Cole-Cole 4 equation using data from Zimmerman Tables S4, S10, S11, and GABRIEL TABLE 1 FOR a PARAMETERS
        # (a parameters for Cole-Cole 4 were not found in the Zimmerman paper)
        elif method == "4ccz":'''


    elif method == "hnz":
        for x in range(len(freqs)):
        
            # Havriliak-Negami Equation with data from Zimmerman Table S6

            perm = tissue_dict["z_s6_ep_inf"] + tissue_dict["z_s6_dep"]/((1 + (1j * freqs[x] * tissue_dict["z_s6_tau"]) ** tissue_dict["z_s6_a"]) ** tissue_dict["z_s6_bet"])

            if not np.isclose(freqs[x], 0):
                perm = perm + tissue_dict["z_s6_sigma"]/(1j * freqs[x] * e0)

            perms[x] = perm


    else:
        raise Exception("\nError: please specify a method from the following:\n4ccg, eq5g, 2ccz, hnz")    


    
    # Calculate conductivities
    
    conds = 1j * np.array(freqs) * perms * e0    
    
    # This if checks if any of the frequencies in freqs are zero.  This case needs to be handled separately because
    # conductivity at a frequency of zero is not zero; multiplying by the frequency
    # makes the frequency term in the denominator of the last term added onto the value
    # in the equation for permittivity, leaving the conductivity as sigma_i/(1j + e0)
    if not all(freqs):
        
        if method == "4ccg":
            conds[conds == 0] = tissue_dict["g_1_sigma"]
        elif method == "eq5g":
            conds[conds == 0] = tissue_dict["g_2_sigma"]
        elif method == "2ccz" or method == "4ccz":
            conds[conds == 0] = tissue_dict["z_s4_sigma"]
        else:
            conds[conds == 0] = tissue_dict["z_s6_sigma"]
        
        
    # If the user didn't specify that they don't want rounding, round.
    if not round_decimals is None:
        perms = perms.round(round_decimals)
        conds = conds.round(round_decimals)
        
        
    # Return the permittivities and conductivities as numpy arrays with complex values
    return perms, conds