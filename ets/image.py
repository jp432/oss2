# -*- coding: utf-8 -*-
#import nibabel as nib
#import dipy as dp

class Image(object):
    def __init__(self, path, ):
        """This class provides basic image and tensor utilities. Importantly, we always assume that the images are in RAS+ space (defined in `nibabel documentation  <https://nipy.org/nibabel/coordinate_systems.html#naming-reference-spaces>`__.) Yet, it is possible to transform any images into the desired space using the internal methods of this class.

        :param path: path to the MRI file
        :type path: string
        """
        self.img = nib.load(path)
        
        pass

    def change_orientation(self, target):
        """
        By overwriting the image affine transformation, flips or exchanges the axes of the according to the target tuple.

        .. note::
            If ``target=['-z','y','x']``, the current positive `x` will be mapped to the negative `z`. `y` axis won't change, and `z` axis will be substituted by the current `x`.

        .. warning::
            The center of frame will not change.
        """
        # outline: make a transformation matrix that simply changes the order of rows and/or negates them. 
        pass

    def change_center(self, target):
        """
        By overwriting the image affine transformation, moves the center of frame the according to the target tuple. 

        .. note:: 
            If ``target=[0.3, 0, -1.2]`` the center will be moved by 0.3 *unit* in *x* direction, and -1.2 *unit* in `z` direction. **Unit** is the physical unit and is inferred from the voxel size unit.

        .. warning::
            The orientation will not change.
        """
        # outline: just change the last column of the affine matrix
        pass


    def get_affine(self):
        """Returns the affine matrix that transforms the voxel (index) space to physical reference frame. 

        .. note:: The reference frame can be anything.

        :return: the transformation that maps the voxel index (unitless) to reference frame (physical space)
        :rtype: numpy array
        """
        return self.img.affine


    def get_inverse_affine(self):
        """Returns the affine matrix that transforms the physical reference frame to the voxel (index) space.
        
        .. note:: The reference frame can be anything.
        """
        # outline
        # check https://nipy.org/nibabel/coordinate_systems.html#applying-the-affine        



    def crop(self, range_x, range_y, range_z, unit='physical'):
        """Returns part of the image that lies in the given ranges. The ranges can be either ``physical`` or ``index`` and must be a list or tuple of length two.
        
        :param range_x: range of x  
        :type range_x: list of floats integers
        :param range_y: range of y 
        :type range_y: list of floats integers
        :param range_z: range of z
        :type range_z: list of floats integers
        :param unit: either 'physical' or 'index`, defaults to 'physical'
        :type unit: str, optional
        """
        # outline: just slice the image if index, otherwise use the inverse affine to find the associated indices.
        pass


    def segment_mri(self, config=None):
        """Segments the MRI image given the associated config with dipy.

        .. note::
            **To Micheal and Philipp**: Feel free to develop is needed.
        """
        pass

    def save(self, path):
        """saves the image on the desired path.
        """
        pass
