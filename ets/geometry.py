# -*- coding: utf-8 -*-

import numpy as np
import os
import pickle

import netgen.occ as occ
from ngsolve import Draw
from ngsolve import Mesh
from ngsolve import TaskManager

from coordinates import Frame
from segged_nifti2TDSS import segged_nifti2TDSS as sn2TDSS

# * Note about how roi geometry is in voxel space*

class Geometry(object):
    
    '''
        All parameters that represent a distance should be entered in units of millimeters.
        
        :param L: length of the entire electrode
        :type L: float
        
        :param D: diameter of the electrode
        :type D: float
        
        :param l: distance between each contact ring
        :type l: float
        
        :param d_zero: distance between end of electrode to beginning of first contact ring minus D/2
        :type d_zero: float
        
        :param d: regular distance interval between contact rings
        :type d: float
        
        :param d_mark: distance between the mark and the ring farthest from the end of the electrode
        :type d_mark: float
        
        :param roi_dim: dimensions of the region of interest rectangle in millimeters
        :type roi_dim: iterable float container
        
        :param nifti_in_path: name of the nifti file from which to load the region of interest in its realtive or absolute path.  The nifti file should be segmented so that each segment has an integer label in the range from 1 to the number of segments inclusive.  A segment label of zero will be treated as empty space.  If you segment with 3DSlicer, the segmented images will automatically be in this format.
        :type nifti_in_path: string
        
        :param seg_data: segmented nifti data.  Segments must be uniquely labeled with integer values from one to the number of segments inclusive, and blank space must be labeled as zero
        :type seg_data: 3D ndarray of ints
        
        :param p_tip: the point at the tip of the electrode in RAS coordinates.  This will also be used as the center for the region of interest cube
        :type p_tip: float tuple
        
        :param p_tail: a point along the central length-wise axis of the electrode in the direction of the electrode's tail in RAS coordinates
        :type p_tail: float tuple
        
        :param N_segs: array of number of segments to break each contact ring into; N_segs[0] corresponds to the ring nearest the end of the electrode
        :type N_segs: int list
        
        :param thetas: array of the degree of the angle separating each segment for each contact ring; thetas[0] corresponds to the ring nearest the end of the electrode.  Note how an entry of 360 degrees would create a space with no contact ring, and an entry of 0 would create a contact ring without segments.
        :type thetas: float list
        
        :param ct_roi_maxh: the maxh value for the meshing on the electrode contacts and the roi.  This function does not set a maxh value for any part of the electrode besides its contacts.
        :type ct_roi_maxh: float
        
        :param rot_mark: the degree the electrode should be rotated around its central axis.  An angle of zero will leave the electrode's mark pointing along the electrode-based frame's i base vector (as currently defined by the coordinates module), and a positive angle will rotate the mark from this position in a counter-clockwise direction.
        
        :param encap_thick: the thickness of the encapsulation layer in millimeters
        :type encap_thick: float
        
        :param brain_l: the diameter of the ellipsoidal brain approximation. Default brain length obtained from winstonmedical.org/human-brain-facts: 167 mm
        :type brain_l: float
        
        :param roi_seg_step_files: list of the names of the step files that together compose the region of interest.  Each step file should represent a different segment.
        :type roi_seg_step_files: string list
        
        :param tiling: when this module generates the region of interest, it breaks it up into chunks to speed the process.  This parameter is the number of chunks along one edge of the region of interest cube (therefore equivalent to the the cube root of the total number of chunks used for the entire region of interest).  Optimal tiling for region of interest of length 50 mm is approximately 5 and 10 for 100 mm and will vary with the region of interest.
        
        .. figure:: images/labeled_electrode.png
        
        .. figure:: images/labeled_contact_ring_cross_section.png
        
        '''

    def __init__(self, L, D, l, d_zero, d, d_mark, roi_dim, nifti_in_path, p_tip = (0, 0, 0), p_tail = (0, 0, 1), N_segs = [], thetas = [], ct_roi_maxh = 0.1, tip_cntct = False, rot_mark = 0, encap_thick = 0, brain_l = 167, tiling = 5, brain_geo_file = None):
        
        self._assert_args(L, D, l, d_zero, d, d_mark, roi_dim, nifti_in_path, p_tip, p_tail, N_segs, thetas, ct_roi_maxh, encap_thick, brain_l, tiling)
        
        self._L = L
        self._D = D
        self._l = l
        self._d_zero = d_zero
        self._d = d
        self._d_mark = d_mark
        self._roi_dim = roi_dim
        self._nip = nifti_in_path
        self._p_tip = p_tip
        self._p_tail = p_tail
        self._N_segs = N_segs
        self._N = len(N_segs)
        self._thetas = thetas
        self._ct_roi_maxh = ct_roi_maxh
        self._encap_thick = encap_thick
        self._brain_l = 167
        self._tiling = tiling
        self._tip_cntct = tip_cntct
        self._rot_mark = rot_mark
        self._brain_geo_file = brain_geo_file
        
        self._R = self._D/2
        self._f = Frame(self._p_tip, self._p_tail)
    
        # Values that could be parameterized but aren't:
        self._outer_mark_height = l
        self._inner_mark_height = self._outer_mark_height / 2
        self._MARK_THETA = np.pi / 6

        
    @classmethod
    def _assert_args(self, L, D, l, d_zero, d, d_mark, roi_dim, nifti_in_path, p_tip, p_tail, N_segs, thetas, ct_roi_maxh, encap_thick, brain_l, tiling):
        
        self._assert_elec_args(L, D, l, d_zero, d, d_mark, p_tip, p_tail, N_segs, thetas, ct_roi_maxh)

        if tiling <= 0 or brain_l <= 0 or min(roi_dim) <= 0:
            raise Exception("Please enter values greater than zero for the tiling, brain_l (length of the model brain), roi_dim (dimensions of the region of interest), and encap_thick (length of the encapsulation layer) parameters")
            
        if encap_thick < 0:
            raise Exception("Please enter a value for the encap_thick parameter (the thickness of the encapsulation layer) that is greater than or equal to 0")
        
        # This check assumes a circular region of interest and circular brain representation centered at (0, 0)
        brain_r = brain_l/2
        if (p_tip[0] < -brain_r or brain_r < p_tail[0]) or (
            p_tip[1] < -brain_r or brain_r < p_tip[1]) or (
            p_tip[2] < -brain_r or brain_r < p_tip[2]):
            raise Exception("The electrode's tip lies outside of the brain; please adjust the brain length (brain_l) or location of the tip of the electrode (p_tip) as necessary.")
            
        if not os.path.exists(nifti_in_path):
            raise Exception("Please enter a valid nifti file in its correct absolute or relative path for the nifti_in_path parameter")
                
    @staticmethod
    def _assert_elec_args(L, D, l, d_zero, d, d_mark, p_tip, p_tail, N_segs, thetas, ct_roi_maxh):
        if L <= 0 or D <= 0 or l <= 0 or ct_roi_maxh <= 0:
            raise Exception("You must enter values for L (electrode length), D (electrode diameter), l (ring/mark length), and ct_roi_maxh (the maxh for the mesh of the electrode's contacts) that are greater than 0")
    
        if d_zero < 0 or d < 0 or d_mark < 0:
            raise Exception("Please do not enter negative parameters for d_zero, d, d_mark, or encap_thick")

        N = len(N_segs)
            
        if N != len(thetas):
            raise Exception("Please make sure that the length of the N_seg and theta arrays match")

        for x in range(N):
            if N_segs[x] < 0 or thetas[x] < 0:
                raise Exception("Please enter N_segs and thetas with only non-negative values")
            if N_segs[x] * thetas[x] > 360:
                raise Exception("Please select ring segments with combined arc lengths less than the circumference of the electrode")
            if not np.isclose(N_segs[x] % 1, 0):
                raise Exception("Please enter only integers as values for the number of segments that each contact ring should have (N_segs)")

        outer_mark_height = l
        if D/2 + d_zero + l * N + d * (N - 1) + d_mark + outer_mark_height > L:
            raise Exception("Please shorten ring and mark parameters to fit on shaft")

        if p_tip == p_tail:
            raise Exception("Please enter p_tip and p_tail tuples that are not identical.")
            

            
    def _generate_electrode(self):
        
        print("Generating electrode geometry")
        
        # The shapes are initially built along the z-axis and then, when finished, are rotated
        # so that the tip is on p_tip and that its central lengthwise axis is in the direction
        # of p_tail - p_tip

        # Start with a cylinder:
        shaft = occ.Cylinder((0, 0, 0), occ.Z, r = self._R, h = self._L - self._R)
        
        # Make a cylinder for the mark ring that's oriented along the direction of the electrode
        # The value by which you multiply d to calculate the mark's base point can't be zero because
        # then it would be like you had a negative ring, which doesn't make sense.  Set the minimum to 0
        # with this if statement
        d_fac = self._N - 1
        if self._N - 1 < 0:
            d_fac = 0
        dist_along_elec = self._R + self._d_zero + self._l * self._N + self._d * d_fac + self._d_mark
        m_base_pnt = occ.Pnt(0, 0, dist_along_elec)
        mark = occ.Circle(m_base_pnt, occ.Z, self._R).Extrude(self._outer_mark_height * occ.Z)
        # Carve out the sqaure mark on the mark ring
        small_mark = occ.Box(
            occ.Pnt(0,
                    -self._R * np.sin(self._MARK_THETA),
                   dist_along_elec + (self._outer_mark_height - self._inner_mark_height)/2),
            occ.Pnt(self._R,
                   self._R * np.sin(self._MARK_THETA),
                   dist_along_elec + (self._outer_mark_height - self._inner_mark_height)/2 + self._inner_mark_height))
        # Make the smaller mark on the mark ring
        mark -= small_mark
        # Put the mark on the shaft
        shaft = occ.Glue([mark, shaft])
        
        
        # Make the hemispherical electrode tip by cutting half of it off with a piece that looks
        # like those blocks you use to chalk pool sticks
        sphere = occ.Sphere(occ.Pnt(0, 0, self._R), self._R)
        block = occ.Box(occ.Pnt(self._R, self._R, 0), occ.Pnt(-self._R, -self._R, self._R))
        chalker = block - sphere
        electrode = shaft - chalker
 
        
        # Make the contact rings
        for x in range(self._N):
            dist_along_elec = self._R + self._d_zero + self._l * x + self._d * x
            r_base_pnt = occ.Pnt(0, 0, dist_along_elec)
            ring = occ.Circle(r_base_pnt, occ.Z, self._R).Extrude(self._l * occ.Z)
            
            # If there are sections to cut out of the ring
            if self._N_segs[x] != 1:
                # Make a rectangle with which to define the electrical contact sections of the ring:
                # Use divide 360 by the number of segments in this contact ring to find the number of degrees it takes for
                # the segment pattern of contact/no-contact to repeat itself while going around the ring
                # Sin is then used with this value to calculate
                # the linear distance between the beginning and end of the arc of the segment
                ro_by = 360 / self._N_segs[x]
                ct_angle = (ro_by - self._thetas[x]) * np.pi/180
                rect_seg_width = self._D * np.sin(ct_angle/2)
                # rect_seg is a rectangle that is rotated around a ring in order to define the segments of the contact ring
                rect_seg = occ.Box(
                    occ.Pnt(-self._R,
                        -rect_seg_width / 2,
                        dist_along_elec),
                occ.Pnt(0,
                        rect_seg_width / 2,
                        dist_along_elec + self._l))

                # Use the rectangle to define the contact segments and then glue them all to the shaft.
                for y in range(self._N_segs[x]):
                    cntct = ring * rect_seg.Rotate(occ.Axis((0, 0, 0), occ.Z), y * ro_by)
                    # This gives each contact segment a unique name.
                    cntct.bc("cnt_"+str(x + 1)+'_'+str(y + 1))
                    # This specifies a parameter about the mesh for this face
                    cntct.maxh = self._ct_roi_maxh
                    electrode = occ.Glue([electrode, cntct])
            
            # If there aren't sections to cut out of the ring
            else:
                ring.bc("cnt_"+str(x + 1)+'_0')
                ring.maxh = self._ct_roi_maxh
                electrode = occ.Glue([electrode, ring])        
             
        
        electrode.mat("electrode")
        if self._tip_cntct:
            electrode.faces[19 + sum(self._N_segs)].bc("cnt_" + str(self._N + 1) + "_0")
            
        electrode = electrode.Rotate(occ.Axis((0, 0, 0), occ.Z), self._rot_mark)
            
        # Rotate electrode to fit the proper location in space by using the electrode-centered coordinate frame
        electrode = self._f.rotate(electrode)

        # Note for debugging: drawing the geometry in this function will distort the returned coordinates
        # for use outside of the function
        self._electrode = electrode
        
        print("Electrode geometry generated")
    
                
    def _generate_domain(self):
        # Generates a TopoDS_Shape that includes the electrode, region of interest, encapsulation layer,
        # and brain representation as according to the class's member variables.

        # This function assumes that the electrode and roi geometry are oriented in the reference frame
        # as it is desired to be in the final simulation 

        # This function assumes the cross section of the electrode geometry is circular

        # The encapsulation layer will only extend as far as the electrode shaft; it is not guaranteed to reach
        # the surface of the brain.  However, the hole in the brain is guaranteed to break the surface of the brain

        self._generate_electrode()
        
        print("Generating domain geometry")

        # First make shapes oriented around the Cartesian z-axis and then
        # rotate to fit p_tip and p_tail when you finish

        # brain_hole is the electrode geometry but with with an extended shaft so that it can be used
        # that is used to carve out a hole in the brain.  The shaft is extended so that it will definitely
        # break through the surface of the brain.
        brain_hole = occ.Cylinder((0, 0, 0), occ.Z, self._R, self._brain_l)
        block = occ.Box(occ.Pnt(-self._R, -self._R, 0), occ.Pnt(self._R, self._R, self._R))
        sphere = occ.Sphere(occ.Pnt(0, 0, self._R), self._R)
        chalker = block - sphere
        brain_hole -= chalker
        # Rotate to the electrode's orientation
        brain_hole = self._f.rotate(brain_hole)


        # encap_hemi is the hemispherical bit of the encapsulation layer and
        # encap_shaft is the cylindrical part of the encapsulation layer
        encap_hemi = occ.Sphere((0, 0, self._R), self._R + self._encap_thick) 
        encap_shaft = occ.Cylinder((0, 0, self._R), occ.Z, self._R + self._encap_thick, self._L - self._D)
        # Filled encap layer is the encapsulation layer but not hollow
        filled_encap_layer = encap_hemi + encap_shaft
        # Rotate to the electrode's orientation
        filled_encap_layer = self._f.rotate(filled_encap_layer)

        
        roi_geo = sn2TDSS(self._nip, np.array(self._p_tip), self._roi_dim, self._tiling) - (filled_encap_layer)
        self._roi = roi_geo
        brain = -1
        if self._brain_geo_file is not None:
            brain = occ.OCCGeometry(self._brain_geo_file).shape
            if (np.array(brain.bounding_box[1]) - np.array(brain.bounding_box[0])).max() < (np.array(self._roi.bounding_box[1]) - np.array(self._roi.bounding_box[0])).max():
                print("Warning: brain geometry loaded from " + self._brain_geo_file + " may be smaller than the region of interest")
        else:
            brain = occ.Sphere(occ.Pnt(0, 0, 0), self._brain_l/2)
        empty_brain = brain - brain_hole
        empty_encap_layer = (filled_encap_layer - brain_hole) * brain
        
        empty_encap_layer.mat("encap_tissue")
        empty_brain.mat("non_roi_brain")
        
        min_curve_maxh = 10
        
        empty_encap_layer.maxh = min_curve_maxh
        self._electrode.maxh = min_curve_maxh
        # Put the carved pieces together
        self._domain = occ.Glue([empty_brain, empty_encap_layer, roi_geo, self._electrode])
        
        print("Domain geometry generated\n")
        
    
    # These functions put on hold because the ngsolve TopoDS_Shape cut (subtraction sign) function isn't working
    '''def swap_electrode(self, l, d_zero, d, d_mark, N_segs = [], thetas = [], ct_roi_maxh = 0.1, tip_cntct = False, rot_mark = 0):
        
        self._assert_elec_args(self._L, self._D, l, d_zero, d, d_mark, self._p_tip, self._p_tail, N_segs, thetas, ct_roi_maxh)
        
        #self._domain = self._domain - self._electrode
        
        self._l = l
        self._d_zero = d_zero
        self._d = d
        self._d_mark = d_mark
        self._N_segs = N_segs
        self._N = len(N_segs)
        self._thetas = thetas
        self._ct_roi_maxh = ct_roi_maxh
        self._tip_cntct = tip_cntct
        self._rot_mark = rot_mark
        
        self._R = self._D/2
        self._f = Frame(self._p_tip, self._p_tail)
    
        # Values that could be parameterized but aren't:
        self._outer_mark_height = l
        self._inner_mark_height = self._outer_mark_height / 2
        
        
        self._generate_electrode()
        self._domain = occ.Glue([self._electrode, self._domain])
        
        
    def rotate_electrode(self, rot_mark):
        
        #self._domain -= self._electrode
        
        self._electrode = self._electrode.Rotate(occ.Axis(self._p_tip, self._f.get_k()), rot_mark)
        self._rot_mark += rot_mark
        
        self._domain = occ.Glue([self._electrode, self._domain])'''
                
    def generate_mesh(self, tm = True):
        '''
        :param tm: specifies whether the function should make use of NGSolve's TaskManager functionality to compute the mesh
        :type tm: boolean
        
        :return: NGSolve mesh of the class that includes the electode, region of interest, and brain representation as specified by the class's member variables
        '''
        self._generate_domain()
        
        print("Generating mesh")
        if tm:
            with TaskManager():
                occ_geo = occ.OCCGeometry(self._domain)
                self._domain_mesh = Mesh(occ_geo.GenerateMesh())
        else:
            occ_geo = occ.OCCGeometry(self._domain)
            self._domain_mesh = Mesh(occ_geo.GenerateMesh())
        
        Draw(self._domain_mesh)
        
        print("Mesh generated\n")
        return self._domain_mesh
    
    def get_domain(self):
        '''
        :returns: the last domain geometry generated by the object
        '''
        try:
            return self._domain
        except:
            raise Exception("Please generate the mesh with this Geometry object before attempting to access its domain geometry")
    
    def get_electrode(self):
        '''
        :returns: the last electrode geometry generated by the object
        '''
        try:
            return self._electrode
        except:
            raise Exception("Please generate the mesh with this Geometry object before attempting to access its electrode geometry")
    
    def get_roi(self):
        '''
        :returns: the last region of interest geometry generated by the object
        '''
        try:
            return self._roi
        except:
            raise Exception("Please generate the mesh with this Geometry object before attempting to access its region of interest geometry")
    
    
    def get_mesh(self):
        '''
        :returns: the last mesh that the object generated for the entire domain
        '''
        try:
            return self._domain_mesh
        except:
            raise Exception("Please generate the mesh with this Geometry object before attempting to access its domain mesh")


            
            
# Made these functions separate from the class so that they could be called separately from it
def save_shape(topoDS_Shape, file_in_path):
    '''
    Saves the provided shape as a pickle file
    
    :param topoDS_Shape: TopoDS_Shape to save
    :type topoDS_Shape: NetGen TopoDS_Shape
    
    :param file_in_path: relative or absolute path to the file in which to save the given shape.  The file must have a pkl extension.
    :type file_in_path:  string
    '''
    with open(file_in_path, "wb") as file:
        pickle.dump(occ.OCCGeometry(topoDS_Shape), file)

def load_shape(file_in_path):
    '''
    Loads the object in the pickle file as a NetGen TopoDS_Shape.  The pickled object must have been pickled as a TopoDS_Shape.
    
    :param file_in_path: relative or absolute path to the file from which to load the given shape.  The file must have a pkl extension.
    :type file_in_path:  string
    
    :returns: TopoDS_Shape
    '''
    with open(file_in_path, "rb") as file:
        return pickle.load(file).shape

def save_occ(occ_geo, file_in_path):
    '''
    Saves the provided OCCGeometry as a pickle file
    
    :param topoDS_Shape: OCCGeometry to save
    :type topoDS_Shape: NetGen OCCGeometry
    
    :param file_in_path: relative or absolute path to the file in which to save the given OCCGeometry.  The file must have a pkl extension.
    :type file_in_path:  string
    '''
    with open(file_in_path, "wb") as file:
        pickle.dump(occ_geo, file)

def load_occ(file_in_path):
    '''
    Loads the object in the pickle file as a NetGen OCCGeometry.  The pickled object must have been pickled as an OCCGeometry.
    
    :param file_in_path: relative or absolute path to the file from which to load the given shape.  The file must have a pkl extension.
    :type file_in_path:  string
    
    :returns: OCCGeometry
    '''
    with open(file_in_path, "rb") as file:
        return pickle.load(file)
    
def save_mesh(mesh, file_in_path):
    '''
    :param mesh: NGSolve Mesh to save
    :type mesh: NGSolve Mesh
    
    :param file_in_path: relative or absolute path to the file in which to save the given mesh.  The file must have a pkl extension.
    :type file_in_path: string
    '''
    save_occ(mesh, file_in_path)
    
def load_mesh(file_in_path):
    '''
    Loads the object in the pickle file as an NGSolve Mesh.  The pickled object must have been pickled as an NGSolve Mesh.  Take care to distinguish between NetGen Meshes and NGSolve Meshes.
    
    :param file_in_path: relative or absolute path to the file from which to load the given mesh.  The file must have a pkl extension.
    :type file_in_path:  string
    
    :returns: NGSolve Mesh
    '''
    return load_occ(file_in_path)

def save_geo(geometry):
    '''
    Saves all components of the given Geometry object (electrode, roi, domain, and mesh) as pickle files in the same folder as geometry.py
    
    :param geometry: TopoDS_Shape to save
    :type geometry: NetGen TopoDS_Shape
    '''
    path_no_ext = geometry._nip.split('.')[0]
    save_shape(geometry._electrode, path_no_ext + "_electrode.pkl")
    save_shape(geometry._roi, path_no_ext + "_roi.pkl")
    save_shape(geometry._domain, path_no_ext + "_domain.pkl")
    save_shape(geometry._domain_mesh, path_no_ext + "_mesh.pkl")