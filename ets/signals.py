# -*- coding: utf-8 -*-

'''
This module provides the functionality for ``make_signal``

``make_signal`` is a function that will generate an array of values that represent the amplitude of the electrode throughout a single period of the signal described by the parameters.

The signals generated follow this pattern:

#. The amplitude rises from 0 until it reaches a magnitude of 1; this increase may occur instantaneously so that the signal starts at a magnitude of 1

#. The amplitude remains at 1 for a period of time; the length of this period may equal 0

#. The amplitude falls until it reaches 0 at the same rate at which it climbed to 1

#. The amplitude remains at 0 for a period of time; the length of this period may equal 0

#. The amplitude falls to a value below 0 at a rate less than or equal to and directly proportional to the rate at which it climbed to 1

#. The amplitude remains at this value for a period of time; the length of this period is less than or equal to and directly proportional to the length of time the amplitude remained at 1

#. The amplitude returns to 0 at the same rate it fell to its negative magnitude

#. The amplitude remains at 0 for the remainder of the period

A labeled example of one period of such a signal is pictured here:

.. figure:: images/labeled_ramp_signal.png

Specifically, the image depicts a signal of the "ramp" variety, named after the ramp shape the signal forms.  We'll return to what the labels mean in a moment.

``make_signal`` generates 3 general varieties of signals: ramp (pictured above), triangular, and rectangle.

Triangle signals fall from their amplitudes as soon as they reach them:

.. figure:: images/labeled_triangular_signal.png

Rectangle signals reach their amplitudes instantaneously:

.. figure:: images/labeled_rectangular_signal.png



.. note:: This function creates a digital signal by sampling an analog signal.  As such, it will run into errors with the aliasing effect.  This is demonstrated by the following example.

	Say we want to have this function generate a digital signal for an analog signal, part of which looks like this:

	.. figure:: images/analog_rectangular_signal.png

	``make_signal`` returns a digital signal that takes samples from the analog signal like this:

	.. figure:: images/sampled_analog_rectangular_signal.png

	As a result, the digital signal appears like this:

	.. figure:: images/sampled_rectangular_signal.png

	It does not appear exactly rectangular, but instead looks like a ramp shape.

	Errors like this are unavoidable when trying to create  digital representation of an analog signal, but the error can 
	be reduced by increasing the samples per period, n.

'''




import numpy as np
import matplotlib.pyplot as plt


class Line():
    '''
    This is a helper functor used to generate a line function.
    After generating it with the proper parameters, it
    can be called with a single x-value to give the
    line's corresponding y-value.
    '''
    
    # Constructor given slope and y-intercept
    def __init__(self, m, b):
        self.m = m
        self.b = b
    
    # Constructor given two points on the line
    def __init__(self, x1, x2, y1, y2):
        self.m = (y2 - y1)/(x2 - x1)
        self.b = self.m * x1 * (-1) + y1
    
    # Get the y-value given an x-value
    def __call__(self, x):
        return self.m * x + self.b

    
# x1 and x2 are the continuous beginning and ends of the areas you want the function to stop
# x1 and x2 are included in the results array if they are integer values
def line_ints(x1, x2, y1, y2, deci_round = 6):
    '''
    This function takes in two points on a line and returns an array of
    the line's y-values for the integer x inputs that lie between the two points.
    
    For example, given points (25.25, 3) and (30.3, 16), the function would return
    the y-values for the line through those two points at x = 26, x = 27, ... x = 30.
    This would look like [4.930693, 7.904950, 10.079208, 12.653465, 15.227723]
    
    You can change the number of decimal places the function rounds too by specifying
    the deci_round parameter.  If left unspecified, it will take a default value of 6.
    
    If x1 or x2 is an integer, it will be included in the array.
    '''
    
    # Make the line function
    fxn = Line(x1, x2, y1, y2)
    
    # Minimum bound on array x-values (inclusive)
    min_bound = int(np.ceil(x1))
    
    # Maximum bound on array x-values (exclusive)
    # If x2 is a decimal, max_bound will be the ceiling of x2
    # If x2 is an integer, max_bound will be the next highest integer
    max_bound = int(np.ceil(x2)) #int(np.ceil(x2) + (1 - np.ceil(x2 % 1)))
    
    # Prepare results array
    results_size = max_bound - min_bound
    results = np.empty(results_size)

    # Calculate and save values
    for x in range(results_size):
        results[x] = round(fxn(min_bound + x), deci_round)
        
    return results
    

def make_signal_helper(pulse_width, full_sig_width, ratio, interpulse_interval, freq, n, deci_round):
    '''
    This function returns an array of size n for the signal described by the parameters.
    This function is written to handle the most general form of the shapes - the ramp shape - and
    therefore is also rectangular and triangular parameters.
    '''
    
    # b is the time it takes to build up to (and equivalently, step down from)
    # the full signal in the pulse width.
    b = (pulse_width - full_sig_width) / 2
    
    # L_LARGE is the positive amplitude of the signal
    L_LARGE = 1
    # l_small is the negative amplitude of the signal
    l_small = -1 * L_LARGE / ratio
     
        
    # Relative proportion each section takes of the total signal;
    # proportions calculated relative to n.
    # Scales are rounded for float rounding errors
    
    scale = freq * n
    
    scaled_pw = round(pulse_width * scale, deci_round)
    scaled_b = round(b * scale, deci_round)
    scaled_fsw = round(full_sig_width * scale, deci_round)
    scaled_ipi = round(interpulse_interval * scale, deci_round)
    scaled_pw_prime = round(scaled_pw * ratio, deci_round)
    scaled_b_prime = round(scaled_b * ratio, deci_round)
    scaled_fsw_prime = round(scaled_fsw * ratio, deci_round)
    
    
    # This is error handling for signals longer than n.
    sig_length = scaled_pw + scaled_ipi + scaled_pw_prime
    if sig_length > n:
        raise Exception("Pulse width, interpulse interval, and recharge pulse length together sum larger than the period")
    
    
    signal = np.zeros(n)
    
    
    # Number of samples from the beginning through the pulse width:
    pw_samps = int(np.ceil(scaled_pw))
    # Number of samples from the beginning through the first build ramp:
    b1_samps = int(np.ceil(scaled_b))
    # Number of samples from the beginning through the end of the full signal width:
    b_fsw_samps = int(np.ceil(scaled_b + scaled_fsw))
    # Number of samples from the beginning through the ipi:
    thru_ipi_samps = int(np.ceil(scaled_pw + scaled_ipi))
    # Number of samples from the beginning up to the beginning of the full prime signal:
    up_to_full_prime_samps = int(np.ceil(scaled_pw + scaled_ipi + scaled_b_prime))
    # Number of samples from the beginning through the end of the full prime signal:
    thru_end_full_prime_samps = int(np.ceil(sig_length - scaled_b_prime))
    
    
    # Filling the full-signal samples:
    signal[b1_samps : b_fsw_samps].fill(round(L_LARGE, deci_round))         
    
    # The ipi is already filled because the signal array is initially filled with zeros
    
    # Filling the full-signal prime samples:
    signal[up_to_full_prime_samps : thru_end_full_prime_samps].fill(round(l_small, deci_round))
    
    # If there is no build-up ramp, don't do calculations for them
    if b1_samps != 0:
        # Filling samples from first build-up:
        signal[:b1_samps] = line_ints(0, scaled_b, 0, L_LARGE, deci_round)
        # Filling samples from build-down:
        signal[b_fsw_samps: pw_samps] = line_ints(scaled_b + scaled_fsw, pw_samps, L_LARGE, 0, deci_round)
        # Filling samples for first prime build-'up':
        signal[thru_ipi_samps : up_to_full_prime_samps] = line_ints(scaled_pw + scaled_ipi, scaled_pw + scaled_ipi + scaled_b_prime, 0, l_small, deci_round)
        # Filling samples for first prime build-'down':
        signal[thru_end_full_prime_samps: int(np.ceil(sig_length))] = line_ints(scaled_pw + scaled_ipi + scaled_fsw_prime + scaled_b_prime, sig_length, l_small, 0, deci_round)
    
    # If the signal is rectangular, set any samples that fall exactly
    # on the signal increase or decrease points to zero (so that the 
    # digital signal starts at zero)
    else:
        signal[0] = 0
        if scaled_pw % 1 == 0:
            signal[b_fsw_samps - 1] = 0
        if (scaled_pw + scaled_ipi) % 1 == 0:
            signal[thru_ipi_samps - 1] = 0
        if (scaled_pw + scaled_ipi + scaled_pw_prime) % 1 == 0:
            signal[thru_end_full_prime_samps - 1] = 0
            
    
    return signal


def make_signal(pulse_width, ratio, interpulse_interval, freq, shape, full_sig_width = 0, deci_round = 6, n = 0, save_img = True):
    '''

:param pulse_width: the time in seconds it takes for the signal to climb to its maximum altitude and fall back to 0; must be non-negative
:type pulse_width: float

:param ratio: the ratio of the maximum magnitude of the positive section of the signal to the maximum magnitude of the negative section of the signal; must be greater than or equal to 1
:type ratio: float

:param interpulse_interval: the interpulse interval; the time in seconds between the positive section of the signal and the negative section of the signal; must be non-negative
:type interpulse_interval: float

:param freq: the frequency of the signal in hertz; must be non-negative
:type freq: float

:param shape: specifies the variety of signal
	\n\t“triangular” means that there is no flat part at the top of the signal
	\n\t“rectangular” means that there is no ramp (build-up signal)
	\n\t“ramp” means that there is a ramp with a flat part at the top of the signal and a build-up/down ramp on both side of the flat part at the top of the signal.  This is the only option where you can specify full_sig_width because with the other shapes, it's unncessary to and therefore not allowed so as to prvent errors.  Also, the full_sig_width specified with the "ramp" shape must be less than the pulse_width; otherwise the signal could not exist.
:type shape: string

:param full_sig_width: the length of time in seconds that the signal remains at its maximum positive amplitude; can only be specified with ramp signals; if left unspecified (as it should be for rectangular and triangular signals), the default is 0; must be non-negative
:type full_sig_width: float

:param deci_round: the number of decimal places the function should round the returned signal array values to; if left unspecified, the default is 6; values too large or too small may lead to faulty signals
:type deci_round: int

:param n: the number of samples per period.  This value is equivalent to the number of elements in the array that this function returns.  The function will take care of this parameter for you if you leave this parameter unspecified, but it will use a low value, meaning the signal it returns will have a low precision.  If you specify this parameter to equal 0, the function will act as if you hadn't specified it at all.  If you specify this parameter but it is too low to produce a sufficiently representative digital signal (the aliasing error is too high), the function will throw an exception, indicating that you need to increase the value of n (given that you keep the rest of your parameters identical).
:type n: int

:param save_img: whether or not to save a plot of the signal as a png file.  The file will be saved in the location where the signals module file is located.  The name of the file will begin with \"signals\" and then contain the full parameters of the signal in the order they're given to the function call, starting with pulse_width and ending with the number of samples in the signal (n).
:type save_img: bool

:returns: two lists of size n.  The elements of the same index in each list refer to the same sample.  For example, the first element of the first list returned and the first element of the second list returned both describe the first sample of the signal.  The first list returned gives the timestamps for the samples, and the second list returned gives the amplitude of the signal at the corresponding timestamp.
   
All parameters must be positive.
    
 
    '''
    
    MIN_SAMPS_PER_SEG = 4
    
    # If the user indicated that they want the function to calculate n
    # Calculate the minimum required n for a sufficiently representative signal
    # (this guarantees there are at least two samples per signal segment)
    if n == 0:
        smallest = pulse_width
        if interpulse_interval > 0:
            smallest = min(smallest, interpulse_interval)
        if full_sig_width > 0:
            smallest = min(smallest, full_sig_width)
            if pulse_width - full_sig_width > 0:
                smallest = min(smallest, (pulse_width - full_sig_width)/2)
        
        # Plus one decreases the likelihood that the samples fall on integers
        # so that the sample is more likely to be representative
        n = int(np.ceil(MIN_SAMPS_PER_SEG/(smallest * freq))) + 1
       
    
    '''
    The minimum duration for a signal segment is CUTOFF
    Using this formula for the cutoff, at least two samples should land on the smallest segment of the
    signal because you would have MIN_SAMPS_PER_SEG samples for every length of b in the period.
    (1/freq)/b is the number of b lengths in the period.  To have one sample per b length,
    you would just need (1/freq)/b samples per period.  Multiply this by 2, and you have
    the number of samples needed in a period to have 2 samples per b length.  To have at least
    2 samples per b length, n would have to be greater than this value, which is
    (2/freq)/b, so b > 2/(freq * n).
    '''
    
    CUTOFF = MIN_SAMPS_PER_SEG/(freq * n)
    
    fsw_name = "Full signal width"
    

    if ratio < 1:
        raise Exception("Ratio parameter must be greater than or equal to 1")
    
    if pulse_width <= 0 or full_sig_width < 0 or interpulse_interval < 0 or freq < 0 or n < 0 or deci_round < 0:
        raise Exception("Please enter only positive parameters")
    
    if pulse_width < CUTOFF:
        raise Exception("Please increase n to generate an accurate output for a signal of this shape and size.")
    
    if interpulse_interval < CUTOFF and interpulse_interval != 0:
        raise Exception("Please increase n to generate an accurate output for a signal of this shape and size.")
    
    
    signal = []                    
    if shape == "triangular":
        if full_sig_width != 0:
            raise Exception("Please do not specify a " + fsw_name + " with the triangular option")
        signal = make_signal_helper(pulse_width, 0, ratio, interpulse_interval, freq, n, deci_round)
    
    elif shape == "rectangular":
        if full_sig_width != 0:
            raise Exception("Please do not specify a " + fsw_name + " with the rectangular option")
        signal = make_signal_helper(pulse_width, pulse_width, ratio, interpulse_interval, freq, n, deci_round)
    
    elif shape == "ramp":
        if full_sig_width >= pulse_width:
            raise Exception("Please make the " + fsw_name + " less than the pulse width")   
        
        # This check ensures that the n will accurately detect the build-up signal.
        # If b is not zero but it's too small relative to the n or
        # if the full_sig_width is too small relative to n
        if ((pulse_width - full_sig_width) / 2 <= CUTOFF) or full_sig_width <= CUTOFF:
            raise Exception("Please increase n to generate an accurate output for a signal of this shape and size.")
        signal = make_signal_helper(pulse_width, full_sig_width, ratio, interpulse_interval, freq, n, deci_round)
    else:
        raise Exception("Please define the shape for the signal as either triangular, rectangular, or ramp")

        
    time_stamps = np.linspace(0, (1/freq), num=n, endpoint = False)
    
    if save_img:
        plt.plot(time_stamps, signal)
        plt.xlabel("time from first sample (s)")
        plt.ylabel("signal amplitude")
        plt.savefig("signal_" + str(pulse_width) + '_' + str(ratio) + '_' + str(interpulse_interval) + '_' + str(freq) + '_' + shape + '_' + str(full_sig_width) + '_' + str(n) + ".png")
        plt.clf()
    
    return time_stamps, signal
        
        
def fft(t, s):
    """Computes the Discrete Fourier Transform (DFT) of a real-valued signal.
    :param t: timestamps of samples
    :type t: numpy.array of type float
    :param s: signal value 
    :type s: numpy.array of type float
    """
    from numpy.fft import rfft, rfftfreq
    
    dt = t[-1]/s.size # sample spacing (inverse of sampling rate)

    fourier_comps = rfft(s)
    fourier_freqs = rfftfreq(s.size, d=dt)
    return fourier_freqs, fourier_comps


def octave_band(omegas, freq, freq_param = 24440):
    '''
    This function uses the octave band method to choose which frequencies generated by the Fourier Transform
    to use in calculations.
    The freq_param value used in OSS-DBS appears to be 24440 Hz (see the OSS-DBS tutorial)
    
    omegas are the frequencies calculated by fft
    
    freq is the frequency of the signal passed into fft
    
    freq param is the frequency that the octave bands start at; the default is 24440 because this is the value
    that was presented for octave band in the OSS-DBS tutorial pdf
    '''
    
    # We're going to use all of the frequencies lower than the given threshold
    omegas_to_keep = list(omegas[omegas < freq_param])

    # Past the given threshold, we're going to use the frequencies that are the centers of octaves
    # This function uses the OSS-DBS implementation to find these values (see Truncation_of_spectrum.py)

    # We assume that the greatest frequency that's lower than the given threshold
    # is the lower bound of the first octave
    lower_bound = omegas[len(omegas_to_keep)-1]
    
    
    #This factor is used in iterating through the center frequencies (you'll see in a moment)
    octave_scale = 1
    
    # The center frequency of an octave band is equal to
    # the lower bound of the octave plus the max frequency of the octave divided by sqrt(2).
    # With this equation,, we can calculate and save the rest of the center frequencies in the provided omegas list
    # by using the while loop, which makes sure we don't exceed the frequencies in omega because they are the only
    # frequencies we can select from
    while (lower_bound + (freq*octave_scale)/np.sqrt(2.0) < omegas[-1]):
        center_freq = lower_bound + (freq*octave_scale)/np.sqrt(2.0)
        omegas_to_keep.append(center_freq)
        octave_scale *= 2

    return omegas_to_keep


'''
This function plots the frequencies selected by the octave band function against all of the frequencies to make a graph that
can be used to compare against the OSS-DBS graph provided in the OSS-DBS tutorial.

orig_omegas - the list of frequency values that the octave band function was provided to select from

ob_omegas - the list of frequency values that the octave band selected

ft_amps - the second list returned by the fft function in signals.py

logscale - whether the plot should be logscaled or not (the one in OSS-DBS is logscaled)
'''
def plot_octave_band(orig_omegas, ob_omegas, ft_amps, logscale = True):
    if logscale:
        plt.xscale("log")
    plt.scatter(orig_omegas, ft_amps, s = 5)
    plt.vlines(ob_omegas, ymin = min(ft_amps), ymax = max(ft_amps), linestyles = "dashed", colors = "red")
    

'''
This function calculates the frequencies for the which conductivities and permittivities should be calculated for use in
the finite element method.

pw - the length of time that the signal is positive in each cycle (units are in seconds)
ratio - the ratio of the time that the signal is negative for recharge to time that the signal is positive (units are in seconds)
ipi - the interpulse interval, the time in seconds between the positive and negative parts of the signal
freq - the frequency of the signal
shape - the shape of the signal

These parameters can be expanded to match the full capability of the make_signal.
'''
def calc_omegas(pw, ratio, ipi, freq, shape, freq_param = 24440):
    # Signal is an array of digital signal amplitudes converted from the analog signal described by the parameters, and 
    # timestamps is a list of the time in seconds from the first sample at which each sample was taken.
    timestamps, signal = make_signal(pw, ratio, ipi, freq, shape)
    
    # See the fft function (defined above) to understand these values
    omegas, ft_amps = fft(timestamps, signal)
    
    # Return the frequencies selected by octave_band
    return octave_band(omegas, freq, freq_param)