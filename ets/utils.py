# -*- coding: utf-8 -*-

# This file is a collection of handy routines accessed in the library.


def refine_mesh():
    """
    Refines mesh with some strategy.
    """
    pass

def custom_precond():
    """
    applies a custom preconditioned to the linear system.
    """
    pass
