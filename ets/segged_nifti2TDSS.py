#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pdb

from ngsolve import *
import netgen.occ as occ
from netgen.webgui import Draw as DrawGeo
from ngsolve.webgui import Draw as Draw

from dipy.io.image import load_nifti
from nibabel.affines import apply_affine


# In[2]:


def segged_nifti2TDSS(nifti_in_path, roi_center_RAS, roi_dim, tiling = 5):
    '''
    This function takes in data from a segmented nifti file and returns its NetGen TopoDS_Shape.
    
    nifti_in_path - relative or absolute path of pre-segmented nifti file.  Each segment should be labeled with an
    integer between 1 and the number of segments inclusive, and empty space should be labeled with a 0.
    
    roi_center_RAS - the center of the ROI cube in RAS coordinate units
    
    roi_dim - the dimensions of the ROI rectangular prism in millimeters
    
    tiling - integer greater than one.  See split_glue below for more details.
    
    rounded_roi - boolean representing whether the roi should be carved by a sphere with diameter equal to the 
    greatest dimension given for the roi.  If false, the ROI will be left as a rectangular prism.  If true and 
    all of the roi's dimensions are equal, the roi should be a sphere with diameter of the given dimensions.
    '''
    
    # The segmented data we want to use is the data variable
    data, affine, img, = load_nifti(nifti_in_path, return_img = True)
    print("Nifti data loaded\n")
    
    
    
    # The following code section identifies the data in the region of interest as specified by the
    # given roi center and roi dimensions
    
    # First, using the center and dimensions of the ROI that were defined by the user in RAS coordinates,
    # we need to find the center and radius of the ROI in voxel coordinates so we can
    # select the roi data from the segmented nifti data, which is in voxel coordinates
    
    
    # Find a point in RAS cordinates that's on the edge of the region of interest
    pnt_on_bound_RAS = roi_center_RAS + np.array(roi_dim)/2
    
    # Convert the boundary point's coordinates from RAS coords to voxel coord by applying the affine
    inverse_affine = np.linalg.inv(affine)
    pnt_on_bound_voxel = apply_affine(inverse_affine, pnt_on_bound_RAS)
    
    # Find the difference in voxels between the point on the boundary and the origin
    roi_center_voxel = apply_affine(inverse_affine, roi_center_RAS)
    roi_rads_voxel = abs(roi_center_voxel - pnt_on_bound_voxel)
    
    
    # begins and ends are the data bounds for the ROI defined by the user
    # We specify .astype(int) so that the interpreter is guaranteed that begins and ends contain
    # integer values for when we use begins and ends for slicing later on
    begins = np.floor(np.array(roi_center_voxel) - roi_rads_voxel).astype(int)
    ends = np.floor(np.array(roi_center_voxel) + roi_rads_voxel + np.ones(3)).astype(int)
    
    
    # Select the nifti data from the region of interest
    roi_data = np.array(data[begins[0]:ends[0], begins[1]:ends[1], begins[2]:ends[2]])
    print("Region of interest selected\n")
    
    
    
    # The voxel dimensions are given in img from when we loaded the nifti data.
    # NiBabel appears to say that the units for these dimensions are millimeters
    vox_dim = np.round(np.array(img.header.get_zooms()[:3]), 6)
    
    
    
    segments = []
    # Given that each segment should be labeled with a unique integer from 1 to the number of segments inclusive,
    # the total number of segments we're expecting to process in this ROI is equal to the max segment number
    # We treat empty space with a label of zero as just that: empty space.
    
    num_segs = round(data.max())
    # We're going to process each segmentation region separately so that we can label each of them with their
    # corresponding material label
    
    for seg in range(num_segs):
    
        # roi_seg_data is the nifti data but only the segment of interest currently being looped over has
        # non-zero values; its labels are converted to the integer 1 and every other value in the array
        # is converted to 0.  We do this because we want to treat every other segment as empty space,
        # and the label given to the segment in roi_seg_data that represents the current segment 
        # doesn't matter for creating the segment's geometry as long as the label's not zero
        roi_seg_data = (roi_data == seg + 1).astype(int)
        print("Segment ", seg + 1, '/', num_segs, " selected")
        
        # If the data for this segment aren't completely zeroes, proceed with processing.
        # If they were all completely zeroes, it would mean that this segment isn't present in the ROI,
        # so we could just not worry about this segment and move on to save time.
        if roi_seg_data.any():
            
            # Generate this segment's geometry with split_glue
            # This geometry is generated with RAS dimensions
            segment = split_glue(roi_seg_data, vox_dim, tiling)
            
            # Label it with its corresponding meterial label and add it to the list of segments generated so far
            segment.bc("segment_" + str(seg + 1))
            segments.append(segment)
            
            #DrawGeo(segment)
            
        print("Segment ", seg + 1, "processed\n")
            
    # If the above for loop does not put any geometries in the segments list, it means that none of the segments are
    # present in the ROI because we skipped processing segments that were't in the ROI data.  This in turn,
    # means that the entire region of interest is completely empty - if it is not filled with segmented brain tissue,
    # it could only be filled with empty space, as represented by the label 0 in the nifti data.
    if len(segments) == 0:
        raise Exception("Blank region of interest")
    
    
    # Glue the shapes together into the ROI
    # Note that the ROI is not in its final position yet.  One of its corners lies on -vox_dim/2 so that the
    # voxel on this corner shares its center with the origin.  I did it this way because, according to NiBabel,
    # the voxel coordinates correspond to the center of the voxel.
    geo = occ.Glue(segments)
    
    # Shave the bit of the roi added for processing whole voxels
    # Potentially useful but seemingly difficult to implement because of how
    # it shaves off of the boundary
    '''
    vox_count = ends - begins
    diff = (vox_count - roi_rads_voxel)/2
    pnta = -vox_dim[0] + diff * vox_dim
    pntb = -vox_dim[0] + (diff + roi_rads_voxel) * vox_dim
    geo *= occ.Box((pnta[0], pnta[1], pnta[2]),
                   (pntb[0], pntb[1], pntb[2]))'''
    
    # This is the center of the roi rectangular prism
    #rect_center = -vox_dim[0] + (diff + np.array(roi_dim)/2) * vox_dim
    rect_center = np.array(roi_data.shape)/2 * vox_dim - vox_dim/2
    
    '''
    # If the roi is supposed to be rounded, carve it to be rounded by using a sphere with diameter
    # equal to the max dimension (height, width, or length) of the current rectangular prism roi.
    # Subtract an extra 0.05 mm off of the sphere's diameter so that it will actually match 
    # the rectangular prism's dimensions; NGSolve's OCC functionality doesn't seem to be very precise with shapes.
    if rounded_roi:
        carver = occ.Sphere(occ.Pnt(rect_center), np.array(roi_dim).astype(float).max()/2.)
        
        # Carve the corners off of the rectangular prism
        geo *= carver'''
    
    
    # The difference between where the ROI is right now and where it's supposed to be is the difference
    # between its center and where its center is supposed to be.
    # move_by defines this difference.
    move_by = np.array(roi_center_RAS) - rect_center
    
    # Move the shape to where it's supposed to be.
    geo = geo.Move((move_by[0], move_by[1], move_by[2]))
    
    print("ROI generated")
    
    # Return the ROI geometry
    return geo


# In[3]:


def split_glue(roi_data, vox_dim, tiling):
    '''
    This function takes in the data from a nifti containing a region that's supposed to be meshed as a single
    segment (which means that this function will treat all voxels with non-zero values the same and
    all voxels with a value of zero the same).
    This function returns an NGSolve mesh of the region.  It does this by forming a NetGen TopoDS_Shape 
    rectangular prism with the dimensions of the inputted nifti array.  It then carves this prism so that the only
    parts of it remaining represent the non-zero voxels (it carves the voxels with values of zero from
    the rectangular prism).
    It could be worthwhile to check whether building non-zero voxels into the region of interest would be more
    efficient than this implementation, which instead carves away the zero-voxels until only the region of interest
    is left behind.
    
    
    roi_data - the nifti data array for the region of interest segment to be meshed.

    vox_dim - an array-like input with the dimensions of the nifti's voxels.  This is only used in this function as a
    parameter for calling the carve_chunk function
    
    tiling - used to break down the data into chunks so that it can process the data faster.The number of chunks the
    data is cut into is equal to tiling ** 3.
    
    
    Note that parallelizing the chunk processing might not actually increase the speed of this function
    because this function makes use of NGSolve's TaskManager while solving.  I think using multiple processes to
    handle chunks of the segment simultaneously would split up the CPU available to the TaskManager, effectively making
    the program slower (because it doesn't access more CPU and only increases the need for communication between processes).
    I tried it out and this is indeed what I found to be the result, but perhaps I'm mistaken and there's a way that
    parallel processing could speed the program's runtime beyond what TaskManager already achieves.
    
    I haven't run into any issues with this, but if you run into memory limitations while running this function,
    try gluing as you go instead of saving all the chunks in a list and then gluing them all together in the end.
    I believe the current implementation is faster, but if you need more memory, you need more memory.
    '''
    
    
    # Split the data into the chunks
    chunk_lengs = (np.array(roi_data.shape) // tiling)
    # Make sure the chunks are three dimensional
    chunk_lengs[chunk_lengs == 0] = 1
    
    chunks = []
    print("Carving segment chunks")
    
    # Go through the given data chunk by chunk and process each chunk
    # Used variable name scheibe (slice, auf Deutsch) because I didn't want to confuse slice with array slicing
    for scheibe in range(tiling - 1):
        for row in range(tiling - 1):
            for col in range(tiling - 1):
                
                # Select the data for the chunk that the for loop is currently iterating over.
                # You can think of (scheibe, row, col) as a sort of coordinate index for each chunk
                # Multiplying by the dimensions of the chunks in voxel units by this index gives
                # which data values belong to the current chunk
                chunk = roi_data[
                        scheibe*chunk_lengs[0] : (scheibe+1)*chunk_lengs[0],
                        row*chunk_lengs[1] : (row+1)*chunk_lengs[1],
                        col*chunk_lengs[2] : (col+1)*chunk_lengs[2]]
                
                # If the chunk isn't completely empty (remember, values of zero represent empty space),
                # proceed with processing.  It's a lot faster to just skip over processing the entire chunk
                # when it's empty rather than going through it.
                if chunk.any():
                
                    # Carve the zero voxels from the chunk and save what's left
                    # The geometry returned by carve_chunk is in RAS coordinate units
                    carved_chunk = carve_chunk(chunk, vox_dim, np.array([scheibe, row, col]) * chunk_lengs)
                    chunks.append(carved_chunk)
    
    placement = -np.ones(3)
    for k in range(3):
        
        o = 2-k
        opo = (3-k) % 3
        opt = (4-k) % 3
        
        for i in range(tiling - 1):
            
            # Leftover "planes"
            for j in range(tiling - 1):
            
                ranges = [[chunk_lengs[0], chunk_lengs[0]],
                [chunk_lengs[1], chunk_lengs[1]],
                [chunk_lengs[2], chunk_lengs[2]]]
            
                ranges[o][0] *= (tiling - 1)
                ranges[o][1] = None
                ranges[opo][0] *= (i)
                ranges[opo][1] *= (i+1)
                ranges[opt][0] *= (j)
                ranges[opt][1] *= (j+1)
            
                chunk = roi_data[
                    ranges[0][0] : ranges[0][1],
                    ranges[1][0] : ranges[1][1],
                    ranges[2][0] : ranges[2][1]]
                
                placement[o] = tiling - 1
                placement[opo] = i
                placement[opt] = j
                
                if chunk.any():
                    carved_chunk = carve_chunk(chunk, vox_dim, placement * chunk_lengs)
                    chunks.append(carved_chunk)
            
            
            # Leftover "lines"
            ranges = [[chunk_lengs[0], chunk_lengs[0]],
                [chunk_lengs[1], chunk_lengs[1]],
                [chunk_lengs[2], chunk_lengs[2]]]
            
            ranges[o][0] *= i
            ranges[o][1] *= (i + 1)
            ranges[opo][0] *= (tiling - 1)
            ranges[opo][1] = None
            ranges[opt][0] *= (tiling - 1)
            ranges[opt][1] = None
            
            chunk = roi_data[    
                ranges[0][0] : ranges[0][1],
                ranges[1][0] : ranges[1][1],
                ranges[2][0] : ranges[2][1]]
            
            placement[o] = i
            placement[opo] = tiling - 1
            placement[opt] = tiling - 1
            
            if chunk.any():
                carved_chunk = carve_chunk(chunk, vox_dim, placement * chunk_lengs)
                chunks.append(carved_chunk)
    
                
    # Leftover "point"
    tmo = tiling - 1
    chunk = roi_data[
    (tmo) * chunk_lengs[0] : ,
    (tmo) * chunk_lengs[1] : ,
    (tmo) * chunk_lengs[2] : ]
    if chunk.any():
        carved_chunk = carve_chunk(chunk, vox_dim, np.array([tmo, tmo, tmo]) * chunk_lengs)
        chunks.append(carved_chunk)
            
        
    print("Segment chunks carved")
    
    
    # We don't have to worry about there being null shapes, which would cause occ.Glue to throw an exception,
    # because we don't pass data for segments that aren't present in the region of interest into this function
    # and because we don't add empty chunks to the list of chunks to glue.
    # The geometry returned has one of its corners on the point -vox_dim/2 so that the voxel on this corner
    # has its center at the origin.  This was done because, according to NiBabel, a voxel's coordinates
    # correspond to its center.
    return occ.Glue(chunks)


# In[13]:


def carve_chunk(roi_chunk, vox_dim, begins_at):
    '''
    This is the function that actually does the carving.
    
    roi_chunk is the roi data for the chunk to be carved
    
    vox_dim is the dimensions of the voxels of the nifti data
    
    begins_at is the voxel coordinates of the corner of the chunk nearest the "origin" of the roi: the voxel
    coordinates where roi_chunk[0][0][0] should go so that it fits in with the rest of the chunks in the list
    in split_glue.  This parameter is not used until the end of this function, when the chunk is
    translated from where it's constructed with one of its corners on the origin to where it belongs with respect
    to the respect to the other chunks
    '''

    pnt_b = np.array(roi_chunk.shape) * vox_dim
    carve_this = occ.Box(occ.Pnt(0, 0, 0), occ.Pnt(pnt_b))
        
    
    # Carving
    
    # These values are used in the for loop to calculate the coordinates of the voxels to carve
    # There are initialized outside of the for loop so that the interpreter doesn't need to repeatedly waste
    # time on reallocating memory each time the variable is declared and deleted (that's at least how it works
    # in C++, I'm not actually too sure if that's how it works in Python too)
    add_ons = np.zeros(3)

    # Use TaskManager because otherwise this could take much longer than it already might
    with TaskManager():
        
        # Go through each slice of the chunk
        for scheibe in range(roi_chunk.shape[0]):
            add_ons[0] = scheibe * vox_dim[0]
            
            # Go through each row of the slice
            for row in range(roi_chunk.shape[1]):
                add_ons[1] = row * vox_dim[1]
                
                # If the entire row is empty (contains voxels with values of zero),
                # save time by just carving out the entire row here.
                if np.all(roi_chunk[scheibe][row] == 0):
                    
                    # We're going to start carving the column from the first voxel in the column
                    add_ons[2] = 0
                    
                    # pnt_b is one vox_dim farther from add_on because this way, it will encompass the entire
                    # voxel column base that we want to carve out.
                    pnt_b = add_ons + vox_dim
                    
                    
                    carve_this -= occ.Box(
                            occ.Pnt(add_ons[0], add_ons[1], add_ons[2]),    
                            # Adding opt_orient_shape[2] * vox_dim[2] takes us to the end of the column
                            occ.Pnt(pnt_b[0], pnt_b[1], pnt_b[2] + roi_chunk.shape[2] * vox_dim[2]))
          
                # If the entire column is not empty, go through it voxel by voxel to check which ones
                # need to be carved and which ones don't.
                else:
                    for col in range(roi_chunk.shape[2]):
                        if not roi_chunk[scheibe][row][col]:
                            add_ons[2] = col * vox_dim[2]
                            pnt_b = add_ons + vox_dim
                            carve_this -= occ.Box(
                                    occ.Pnt(add_ons[0], add_ons[1], add_ons[2]),
                                    occ.Pnt(pnt_b[0], pnt_b[1], pnt_b[2]))
        
    # move_by is a vector that represents the difference between where the cube was constructed and where
    # split_glue is expecting to be.
    move_by = begins_at * vox_dim - vox_dim/2
    
    # Move the chunk to its proper location in the entire region of interest
    carve_this = carve_this.Move((move_by[0], move_by[1], move_by[2]))
    
    # Return the chunk
    return carve_this


