# -*- coding: utf-8 -*-
# IMPORTANT: The following are JUST recommended signatures for the functions that isolate tasks from one another. I advise to to merge these functions together. Yet, note that the inputs and outputs are not set carefully. If you feel it is more convenient, you can pass all arguments from one function to another, or transform the whole script in a class that keeps the computed object as attributes and the following functions as methods. 

from mpi4py import MPI
import numpy as np
from ngsolve import *


def load_mesh(mesh_path):
    """loads the mesh from the given path.

    :param mesh_path: path to the mesh file
    :type mesh_path: string
    :return: ngsolve mesh object
    :rtype: ngsolve mesh object
    """
    return Mesh(mesh_path)


def get_complex_conductivity(freq, mesh):
    """Computes the complex conductivity defined as:

    .. math::
        \hat \sigma = \sigma(\omega) + i\omega \epsilon_0\epsilon_r(\omega) 

    and returns an NGsolve CoefficientFunction. 


    :param sim: An instance of `Simulation` class
    :type sim: object
    :param freq: Frequency at which FEM must be solved (in Hz)
    :type freq: float
    :return: complex conductivity as Ngsolve CoefficientFunction over the mesh
    :rtype: CoefficientFunction
    """

    # in SI
    # TODO: the values must be computed using the conductivity module
    sigs = {"WM" : 1, 'GM': 1, 'CSF':1} # [S/m]
    epss = {"WM" : 1, 'GM': 1, 'CSF':1} # relative (unitless)
    eps0 = 8.8541878128e-12 # permittivity of the vacuum 
    
    sigma = CoefficientFunction([sigs[mat] for mat in mesh.GetMaterials()]) 
    eps = CoefficientFunction([epss[mat]*eps0 for mat in mesh.GetMaterials()]) 
    
    return sigma + 2*np.pi*freq*1j*eps

def setup_weak_form(mesh, method, freq, z= None, order=2,
    dirichlet_bcs=".*", contacts = None):
    """Constructs the weak form. Two methods are available:

    #. **Shunt model** (``"shunt"``): The contacts will be modelled with zero surface impedance. We use a mixed-formulation of the Poisson with Lagrange multipliers. Refer to `Dardé & Staboulis 2016 <https://www.esaim-m2an.org/articles/m2an/abs/2016/02/m2an150023/m2an150023.html>`__.

     
    .. math::
        \int_{\Omega} \hat \sigma grad(u) \cdot grad(v) +
        \sum_{contact} \int_{\partial \Omega _{contact}} \mu (u-u_{fix}) + \lambda (v-v_{fix}) =  
        \int_{\Omega} 0\  dx + \sum_{contact} I_{contact} v_{fix, contact}

    #. **Full Electrode** (``"full"``): Contacts will have a non-zero surface impedance which must be specified by the user. Again a mixed-formulation will be used. Refer to `Somersalo et al 1992 <https://epubs.siam.org/doi/10.1137/0152060>`__.
    
    .. math::
        \int_{\Omega} \hat \sigma grad(u) \cdot grad(v) +
        \sum_{contact} \int_{\partial \Omega _{contact}} (u-u_{fix,contact})\cdot(v-v_{fix,contact})/z_{contact} =  
        \int_{\Omega} 0\  dx + \sum_{contact} I_{contact} v_{fix, contact}
    
    
    :param mesh: mesh
    :type mesh: ngsolve mesh 
    :param method: weak formulation method ("shunt" or "full")
    :type method: string
    :param freq: frequency (in Hz)
    :type freq: float
    :param z: surface impedance of the contact, defaults to None. Necessary for the full electrode model (``method="full"``)
    :type z: complex float, optional
    :param order: degree of finite element basis, defaults to 2
    :type order: int > 0, optional
    :param dirichlet_bcs: boundaries markers as described by `NGsolve documentation <https://docu.ngsolve.org/latest/how_to/howto_dirichlet.html>`__ , defaults to ``".*"``
    :type dirichlet_bcs: str, optional
    :param contacts: boundaries markers as described by `NGsolve documentation <https://docu.ngsolve.org/latest/how_to/howto_dirichlet.html>`__, defaults to None
    :type contacts: string, optional
    :raises NotImplementedError: If invalid methods are entered.
    :return: weak form as a dictionary 
    :rtype: dict
    """
    sigma = get_complex_conductivity(mesh, freq)
    complex = True # this must be inferred from the simulation type EQS vs. ES
    if method == "full":
        
        V = H1(mesh, order=order, dirichlet=dirichlet_bcs, complex=complex)
        lm = SurfaceL2(mesh, order=order-1, definedon=mesh.Boundaries(contacts), complex=complex)
        Vfixed = NumberSpace(mesh, order=0, definedon=mesh.Boundaries(contacts), complex=complex)

        # compound space
        fes = FESpace([V, Vfixed, lm])
        fes = CompressCompound(fes)

        # bilinear and linear forms
        u, ufix, lam = fes.TrialFunction()
        v, vfix, mu = fes.TestFunction()
        
        a = BilinearForm(fes)
        f = LinearForm(fes)
        
        a += sigma * InnerProduct(grad(u),grad(v)) * dx
        f += 0* v * dx
        
        # TODO: based oin the contacts string and mesh.GetBoundaries() must look for all the possible contacts and push the followings within a loop.
        a += (u * mu + v * lam) * ds("contact")
        a += -(ufix * mu + vfix * lam) * ds("contact")
        f += 1 * vfix * ds("contact") # depends on desired terminal
    
    elif method=="shunt":
        assert z!=None 
        
        V = H1(mesh, order=order, dirichlet=dirichlet_bcs, complex=complex)
        Vfixed = NumberSpace(mesh, order=0, definedon=mesh.Boundaries(contacts), complex=complex)

        # compound space
        fes = FESpace([V, Vfixed])
        fes = CompressCompound(fes)

        # bilinear and linear forms
        u, ufix = fes.TrialFunction()
        v, vfix = fes.TestFunction()
        
        a = BilinearForm(fes)
        f = LinearForm(fes)
        
        a += sigma * InnerProduct(grad(u),grad(v)) * dx
        f += 0* v * dx
        
        # TODO: based oin the contacts string and mesh.GetBoundaries() must look for all the possible contacts and push the followings within a loop.
        a += InnerProduct(u-ufix, v-vfix)/z * ds("contact")
        f += 1 * vfix * ds("contact") # depends on desired terminal
    
    else:
        raise NotImplementedError("Weak form method not recognized. Only 'shunt' or 'full' are possible.")
    
    a.Assemble()
    f.Assemble()

    weakform = { "fes":fes, "a": a, "f": f}
    if method=="shunt":
        weakform["trial_fns"] =(u, ufix, lam)
        weakform["test_fns"]=(v, vfix, mu)
    else:
        weakform["trial_fns"] =(u, ufix, lam)
        weakform["test_fns"]=(v, vfix, mu)
    
    return weakform

def setup_bcs(diriclets, fes):
    """Sets up the Dirictlet boundary conditions according to the configuration file.
    
    :param diriclets: a dictionaries entailing the values of dirichlet BCs as a dictionary. The keys are boundary conditions' markers.
    :type diriclets: dict
    """
    
    # cf = mesh.BoundaryCF({"wire": 0}, default=-1)
    # gfu = GridFunction(fes)
    ##gfu.components[0].Set(cf, BND)
    pass

def setup_solver(solver_args):
    """Sets up a solver (either PETSc or native NGsolve) for matrix inversion.

    :param solver_args: solver configuration
    :type solver_args: dict
    """
    solver = None # TODO
    return solver

def solve():
    """Inverts the linear system and returns the solution
    """
    # TODO:

    ## outline of native ngsolve for direct inversion
    # res = f.vec.CreateVector()
    # res.data = f.vec - a.mat * gfu.vec
    # gfu.vec.data += a.mat.Inverse(fes.FreeDofs(), inverse="mumps") * res
    
    return None 

def compute_z():
    """Computes the impedance of the system from a specific terminal by dividing the floating potential value of a contact by its total current. These values must be saved for each frequency and terminal. 
    """
    # TODO

    ## outline
    # n = specialcf.normal(self.mesh.dim)  # normal
    # flux = -BoundaryFromVolumeCF(sigma*InnerProduct(grad(gfu.components[0]),n))
    
    # V_contact = Integrate(gfu.components[0], mesh, BND, definedon=mesh.Boundaries("contact") )
    # I_contact = Integrate(flux, mesh, BND, definedon=mesh.Boundaries("contact") )
    # A_contact = Integrate(InnerProduct(n,n), mesh, BND, definedon=mesh.Boundaries("contact") )
    
    # Z = (V_contact/A_contact)/I_contact
    pass

def save_sol():
    """saves the solution of the finite element as a numpy array on the disk. These solutions will be read for the inverse Fourier transform to compute the complete spatiotemporal field distribution.
    """
    pass

def parallel_fem(config):
    """This function solves the FEM problem for each frequency and saves the field solution and the impedance on the disk.
    """

    comm = MPI.Comm.Get_parent()
    rank = comm.Get_rank()
    nproc = comm.Get_size()
        
    mesh = load_mesh()
    
    # must be set according to the sim. the strategy of frequency selection determines the freqs
    if config['ifft_method'] == "complete":
        # freqs are multiplicaitons of the base frequency from 1 to len(signal)
        freqs = np.arange(0,N)*130. # base freq.

    elif config['ifft_method'] == "interpolate":
        # a few frequencies will be sampled, and the transfer function on the fft frequencies will be interpolated -- similar to C. Schmidt 2018. Here N is the number of desired samples. Note that if this options is selected, the impedances must be later interpolated via a post-processing step.
        freqs = np.logspace(0,12,N) * 130 # 
    else:
        raise

    freqs_per_rank = len(freqs)//nproc

    
    impedances = np.zeros(freqs_per_rank, dtype=complex)
    for rep in range(freqs_per_rank):

        freq = freqs[rank*freqs_per_rank + rep]
        weakform = setup_weak_form(mesh, freq, method="full")
        setup_bcs(mesh, weakform["fes"])
        solver = setup_solver(sim.solver_args)    
        solve(weakform, solver)
        
        impedances[rep] = compute_z()

    np.savetxt('impedances_{}.csv'.format(str(rank)), impedances)


if __name__=="__main__":
    parallel_fem()