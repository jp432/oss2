#!/usr/bin/env python
# coding: utf-8

# In[5]:


import numpy as np
import pytest
import math
from itertools import permutations as permute
from itertools import combinations_with_replacement as comb_r

import netgen.occ as occ

import sys
import importlib  
sys.path.append("../ets")

from coordinates import Frame as f


# In[6]:


def distance(coords_1, coords_2):
    # Takes in two array/lists of two points and returns the distance between them.
    # Should work with 2D and 3D tickets.
    # Assumes len(coord_1) == len(coord_2)
    
    sq_sum = 0
    for comp in range(len(coords_1)):
        sq_sum += (coords_2[comp] - coords_1[comp]) ** 2
        
    return math.sqrt(sq_sum)


# In[13]:


def f_assertions(p_tip, p_tail, tol = 0.00001):
    
    print(p_tip, p_tail)
    
    ec = f(p_tip, p_tail)

    i_ray = np.array(ec.get_i())
    j_ray = np.array(ec.get_j())
    k_ray = np.array(ec.get_k())


    # Assert that the i getter functions all access the same values
    assert ec.get_i()[0] == ec.get_i('x')
    assert ec.get_i()[1] == ec.get_i('y')
    assert ec.get_i()[2] == ec.get_i('z')

    # Assert that the j getter functions all access the same values
    assert ec.get_j()[0] == ec.get_j('x')
    assert ec.get_j()[1] == ec.get_j('y')
    assert ec.get_j()[2] == ec.get_j('z')

    # Assert that the k getter functions all access the same values
    assert ec.get_k()[0] == ec.get_k('x')
    assert ec.get_k()[1] == ec.get_k('y')
    assert ec.get_k()[2] == ec.get_k('z')

    # Assert that k points in the right direction
    mag = distance(p_tip, p_tail)
    assert math.isclose(k_ray.dot(np.array((p_tail[0] - p_tip[0], p_tail[1] - p_tip[1], p_tail[2] - p_tip[2]))), mag)

    # Assert that all of the electrode-centered basis vectors are perpendicular to one another
    assert np.isclose(i_ray.dot(np.array(ec.get_j())), 0)
    assert np.isclose(j_ray.dot(np.array(ec.get_k())), 0)
    assert np.isclose(k_ray.dot(np.array(ec.get_i())), 0)

    # Assert that all of the electrode-centered basis vectors have a magnitude of 1
    assert np.isclose(distance([0, 0, 0], ec.get_i()), 1)
    assert np.isclose(distance([0, 0, 0], ec.get_j()), 1)
    assert np.isclose(distance([0, 0, 0], ec.get_k()), 1)

    # Asssert that the i basis vector of the electrode-centered coordinate system 
    # is parallel to the reference xy-plane
    assert np.isclose(ec.get_i('z'), 0)

    # Tests whether i has the correct orientation
    # If the electrode is oriented with a y component greater than 0, the x component of its i basis vector
    # should be less than 0.
    if ec.get_k('y') > 0:
        assert ec.get_i('x') < 0
    # If the electrode is oriented with a y component less than 0, the x component of its i basis vector should be
    # greater than 0.
    elif ec.get_k('y') < 0:
        assert ec.get_i('x') > 0
    # In the following cases, we know ky is zero because it's neither greater than nor less than zero
    # If the electrode is oriented entirely along the reference z-axis,
    # the electrode's i basis vector is arbitrarily defined as (1, 0, 0)
    elif ec.get_k('x') == 0:
        assert ec.get_i('x') == 1
    # If the electrode's central lengthwise axis falls entirely within the zy plane and has a positive x component,
    # the electrode's i basis vector should have a y-value equal to 1
    elif ec.get_k('x') > 0:
        assert ec.get_i('y') == 1
    # If the electrode's central lengthwise axis falls entirely within the zy plane and has a negative x component,
    # the electrode's i basis vector should have a y-value equal to -1
    elif ec.get_k('x') < 0:
        assert ec.get_i('y') == -1
    
    # Tests whether the electrode's j is k x i because that's what it's defined to be
    cross = np.cross(k_ray, i_ray)
    assert math.isclose(ec.get_j('x'), cross[0], abs_tol = tol)
    assert math.isclose(ec.get_j('y'), cross[1], abs_tol = tol)
    assert math.isclose(ec.get_j('z'), cross[2], abs_tol = tol)
        
    # Tests whether to_ref and to_elec are inverses of each other
    p_prime = ec.to_ref(ec.to_elec(p_tip))
    assert math.isclose(p_prime[0], p_tip[0], abs_tol = tol)
    assert math.isclose(p_prime[1], p_tip[1], abs_tol = tol)
    assert math.isclose(p_prime[2], p_tip[2], abs_tol = tol)
    p_prime = ec.to_elec(ec.to_ref(p_tip))
    assert math.isclose(p_prime[0], p_tip[0], abs_tol = tol)
    assert math.isclose(p_prime[1], p_tip[1], abs_tol = tol)
    assert math.isclose(p_prime[2], p_tip[2], abs_tol = tol)
    

def conv_transform(p_tail, pnts, tol = 0.00001):
    # Tests only the frame rotation part of the to_ref conversion function by keeping p_tip at (0, 0, 0).
    # This makes it so that the frames share the same origin, so the transformation between them
    # is just a rotation.
    # Note how in conjunction with the inverse test above, this also tests the to_elec transformation.
    # Points in the pnts parameter are arrays rather than tuples.
    
    # This test will only work if all of the values are non-zero because otherwise the transformation would not
    # be full rank
    if p_tail[0] != 0 and p_tail[1] != 0 and p_tail[2] != 0:
        # Make the electrode frame with its origin on the reference origin
        origin_ec = f((0, 0, 0), p_tail)
        # This is the transformation matrix used to convert reference points to electode coordinates.
        transform_matrix = np.transpose(np.array([np.array(origin_ec.get_i()), np.array(origin_ec.get_j()), np.array(origin_ec.get_k())]))
        
        for tran_this in pnts:
            # This is the translated point as according to to_elec
            tc_tran = origin_ec.to_ref(tran_this)
            # This is the translated point as according to the transformation matrix
            tm_tran = transform_matrix.dot(tran_this)
            
            # Assert that the to_elec function and the transformation matrix agree
            assert math.isclose(tc_tran[0], tm_tran[0], abs_tol = tol)
            assert math.isclose(tc_tran[1], tm_tran[1], abs_tol = tol)
            assert math.isclose(tc_tran[2], tm_tran[2], abs_tol = tol)


def conv_translate(p_tip, p_tail):
    # Tests only that points are translated correctly by the to_ref conversion function.
    # If this test and the conv_transform test both work, the entire to_ref function should work
    # because it means both the translation and transformation work
    # Note how in conjunction with the inverse test above, this also tests the to_elec transformation.
    p_new = f(p_tip, p_tail).to_ref((0, 0, 0))
    assert np.isclose(p_new[0], p_tip[0])
    assert np.isclose(p_new[1], p_tip[1])
    assert np.isclose(p_new[2], p_tip[2])
    

# There is no test for mark angle rotation yet because the sepcifications for the transformation are still unknown.


def rotate_test(p_tip, p_tail, pnts, tol = 0.07):
    # Tests whether rotate correctly rotates a point to match the electrode's orientation in the refernce frame
    # by comparing where it puts shapes to where to_ref would say the points should be put
    
    ec = f(p_tip, p_tail)
    
    for pnt in pnts:
        pnt_to_rot = occ.Sphere(occ.Pnt(pnt), r = 0.1)
        rot_pnt = ec.rotate(pnt_to_rot)
    
        to_ref_pnt = ec.to_ref(pnt)
        
        # Tolerance for this test is so high because the OCC Rotate function seems to not be very percise
        assert math.isclose(rot_pnt.center.x, to_ref_pnt[0], abs_tol = tol)
        assert math.isclose(rot_pnt.center.y, to_ref_pnt[1], abs_tol = tol)
        assert math.isclose(rot_pnt.center.z, to_ref_pnt[2], abs_tol = tol)


# In[15]:


def gen_unit_pnts():
    # Returns a list of every possible combination of points in list form with the values 1, 0, and -1
    
    pnts = []
    for i in range(3):
        for j in range(3):
            for k in range(3):
                pnts.append([i - 1, j - 1, k - 1])
        
    return pnts


# In[16]:


def gen_rand_pnts(num_rand):
    return np.random.randint(-10, 10, (num_rand, 3))

# Concatenated the unit_pnts and rand_pnts lists with extra steps because otherwise the
# shapes weren't identical for some reason.
def get_pnts(num_rand):
    unit_pnts = gen_unit_pnts()
    rand_pnts = gen_rand_pnts(num_rand)
    for pnt in rand_pnts:
        unit_pnts.append(pnt)
        
    return unit_pnts


# In[11]:


def test_no_except(num_rand = 50):
    pnts = get_pnts(num_rand)
    
    for i in range(len(pnts) - 1):
        conv_transform(pnts[i], pnts)
        for j in range(len(pnts) - i -1):
            # p_tip and p_tail cannot be the same point; otherwise, the Frame constructor will raise an exception.
            if pnts[i][0] != pnts[i + j + 1][0] and pnts[i][1] != pnts[i + j + 1][1] and pnts[i][2] != pnts[i + j + 1][2]:
                f_assertions(pnts[i], pnts[i + j + 1])
                f_assertions(pnts[i + j + 1], pnts[i])
                conv_translate(pnts[i], pnts[i + j + 1])
                conv_translate(pnts[i + j + 1], pnts[i])
                # Just using pnts here because it's a set of points.
                # There is perhaps a set of pnts for effective for testing than these pnts
                rotate_test(pnts[i], pnts[i + j + 1], pnts)
                rotate_test(pnts[i + j + 1], pnts[i], pnts)


# In[12]:


# Exception-throwing tests to check whether ec is properly testing the parameters passed into its functions
# There's probably a better way to write this, but I'm not sure how yet
def test_except():
    # Constructor exceptions for p_tip and p_tail being same point
    with pytest.raises(Exception):
        f([0, 0, 0], [0, 0, 0])
    with pytest.raises(Exception):
        f[[1, 1, 1], [1, 1, 1]]
        
    # Constructor exceptions for p_tip and p_tail with the incorrect number of dimensions
    with pytest.raises(Exception):    
        f[[1, 1, 1], [1, 1]]
    with pytest.raises(Exception):
        f[[1, 1], [1, 1, 1]]
    with pytest.raises(Exception):
        f[[1, 1], [1, 1]]
    with pytest.raises(Exception):
        f[[1, 1, 1], [1]]
    with pytest.raises(Exception):
        f[[1], [1, 1, 1]]
    with pytest.raises(Exception):
        f[[1], [1]]

    # Getter exceptions for entering an unsupported parameter
    test = f([0, 0, 0], [1, 1, 1])
    with pytest.raises(Exception):
        test.get_i('b')
    with pytest.raises(Exception):
        test.get_j('b')
    with pytest.raises(Exception):
        test.get_k('b')
    with pytest.raises(Exception):
        test.get_p_tip('b')

    # to_elec exceptions for coordinate parameters with an incorrect number of dimensions
    with pytest.raises(Exception):
        test.to_elec((0, 0))
    with pytest.raises(Exception):
        test.to_elec((1))
    with pytest.raises(Exception):
        test.to_elec((0, 0))
    with pytest.raises(Exception):
        test.to_elec((1))

    # to_elec exceptions for coordinate parameters with an incorrect number of dimensions
    with pytest.raises(Exception):
        test.to_ref((0, 0))
    with pytest.raises(Exception):
        test.to_ref((1))
    with pytest.raises(Exception):
        test.to_ref((0, 0))
    with pytest.raises(Exception):
        test.to_ref((1))