#!/usr/bin/env python
# coding: utf-8

# In[16]:


import numpy as np
import pandas as pd
import pytest

import sys
import importlib  
# setting path
sys.path.append('../ets')

from dielectrics import comp_perm_cond


# freq units are rad/s

def test_dielectrics():
    
    '''    
    The csv with the test values must be formatted with the column headers along the first row of the csv, and
    the column names must match the dataframe indices used in the code.
    
    Each row in the csv should represent a different frequency at which to test the dielectric functions.  The units are in
    radians, so for compatability with the way the csv file is read, each frequency should be in terms of pi by
    writing "pi" at the end of the frequency.  The characters remaining besides pi should form an integer or float.
    It is also assumed that the first frequency in the column will be 0.

    Complex test values should be entered with the format: real_num + imag_numi
    where imag_numi could look something like 5.0i or 5i
    
    The first csv column, "methods", contains the different methods (4ccg, hnz, etc) for which the dielectrics module offers
    functionality.
    
    The second csv column, "tissues", contains the abbreviations used in the code for different tissue types ("gm" for grey
    matter, "wm" for white matter, etc.)
    
    The length of every column must be the same.  That is, if you have different amounts of test frequencies, methods, and tissue 
    types, the column with fewer entries must be extended with empty entries so that the column lengths match.
    This is a result of how pandas dataframes need columns of equal length.  An example of this can be seen in the current 
    dielectric_test_values csv, where there are fewer methods than test frequencies, so the "methods" column is extended with 
    empty entries (commas without anything between them) so that it matches the length of the other columns.
    
    There need to be two columns of expected values for each unique combination of tissue and method: one that starts with
    "cond_expected_" followed by the method abbreviation, followed by underscore tissue abbreviation, and another that starts
    with "perm_expected_w0_" followed by the method abbreviation, followed by underscore tissue abbreviation.  The entries
    in the cond_expected columns should be the expected values of the conductivity for the given method and tissue type for
    the frequency in the corresponding row.  There should only be one entry in the perm_expected_w0 columns, and it should
    correspond to the expected permittivity for the given method and tissue type at a frequency of 0.  This is the only 
    permittivity value tested because the conductivities for all other frequencies are computed based off of the
    values calculated by the permittivity function.  Therefore, if the permitivities were wrong for frequencies other than 0, the
    conductivity tests would probably reflect this.  However, if the conductivity code was rewritten so that it no longer
    depended on the permittivty calculation function, the permittivities for all frequencies would need to be tested individually.
    
    The columns do not need to be in any paarticular order, but I highly recommend it remain organized for readability.
    Currently, the columns start with methods, then tissues, and then test frequencies, and the rest of the columns alternate
    between expected_cond and expected_perm_w0 for the available methods in the order listed in "methods" and combined with the
    tissues in the order listed in "tissues".  If this is confusing, it might be easier to just look at the csv file.
    
    The csv file can be opened in Excel or an equivalent software for better readability, but make sure you save any changes
    you make in csv format and be sure that test_dielectrics uses that file name.
    
    The test value csv should be in the test folder, but the tests will also function if it is instead in the ets folder
    because the ets folder is added to the system path above with sys.path.append('../ets')'''
    
    # Read in the dielectric test values from the csv
    test_df = pd.read_csv("dielectric_test_values.csv")
    
    test_freqs = test_df["test_freq_vals"]
    # Read in the test frequency values as floats, making sure to factor scratch out the pi and factor it into the float value
    test_freqs = [test_freq.replace("pi", '') for test_freq in test_freqs]
    test_freqs = [float(test_freq) * np.pi for test_freq in test_freqs]
    
    # Go through each method listed in the methods column
    for method in list(test_df["methods"]): 
        # Make sure there's actually a method listed in the entry so that it's not just an empty column used to make all 
        # the column lengths in the pandas dataframe equal
        if isinstance(method, str):
        
        # Go through each tissue abbreviation listed in the tissues column
            for tissue in list(test_df["tissues"]):
                # Make sure there's actually a tissue listed in the entry so that it's not just an empty column used to make all 
                # the column lengths in the pandas dataframe equal
                if isinstance(tissue, str):
                
                    # Calculate the permittivities and conductivities based on the method and tissue currently selected
                    p, calc_c = comp_perm_cond(method, tissue, test_freqs)
                    # Assumes 0 is the first element in test_freqs
                    calc_p_w0 = p[0]

                    # Read in the expected conductivity values for this method and tissue from the csv file
                    expected_c = list(
                        test_df["cond_expected_" + method + '_' + tissue].str.replace('i','j').apply(lambda x: complex(x)))

                    # Read in the expected permittivity value at omega = 0 for this method and tissue from the csv file
                    expected_p_w0 = [float(test_df["perm_expected_w0_" + method + '_' + tissue][0])]

                    # Assert that the permittivities and conductivities calculated by the function match the expected values
                    assert calc_p_w0 == expected_p_w0

                    for x in range(6):
                        assert calc_c[x] == expected_c[x]