#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pytest
from abc import ABC

import sys
import importlib
import csv
sys.path.append("../ets")

from signals import make_signal as ms
    

'''
These pytest tests do NOT check the accuracy of the calculation of the areas where the signal should change (in the test file, these calculations are performed in the calc_bonunds function; you can see that in calc_bounds, they are almost identical to the calculations performed by make_signal_helper in make_signal).

Also be aware that extremely high or low decimal precision values can cause tests to fail where they shouldn't because the expected values change based on rounding.  In these tests, the default decimal precision is set to 10 decimal places, which appears to work in most cases.

Does not directly check whether min segs per sample is met because the math already
demonstrates it should work.
'''
    
class T_Signal(ABC):
    '''
    This is an abstract class meant to hold the values and functions
    used for testing values around signals' joints in their most general case
    '''
    
    # The implemented tests do NOT check these calculations 
    def _set_bounds(self):
        '''
        This is a helper function for the constructor.  This section was separated from 
        the rest of the constructor so that it could be overridden by children classes
        (like T_Rect_Signal) where the bounds are unique.
        '''
        
        # b is the time it takes to build up to and step down from the full signal in the pulse width.
        self._b = (self._pw - self._fsw) / 2

        # Number of samples that should be allocated to each section
        # Scales are rounded for float rounding errors
        
        scale = self._freq * self._n

        scaled_pw = round(self._pw * self._scale, self._deci_round)
        scaled_b = round(self._b * self._scale, self._deci_round)
        scaled_fsw = round(self._fsw * self._scale, self._deci_round)
        scaled_ipi = round(self._ipi * self._scale, self._deci_round)
        scaled_pw_prime = round(scaled_pw * self._ratio, self._deci_round)
        scaled_b_prime = round(scaled_b * self._ratio, self._deci_round)
        scaled_fsw_prime = round(scaled_fsw * self._ratio, self._deci_round)

        sig_length = scaled_pw + scaled_ipi + scaled_pw_prime


        # Number of samples from the beginning through the pulse width:
        self._pw_samps = int(np.ceil(scaled_pw))
        # Number of samples from the beginning through the first build ramp:
        self._b1_samps = int(np.ceil(scaled_b))
        # Number of samples from the beginning through the end of the full signal width:
        self._b_fsw_samps = int(np.ceil(scaled_b + scaled_fsw))
        # Number of samples from the beginning through the ipi:
        self._thru_ipi_samps = int(np.ceil(scaled_pw + scaled_ipi))
        # Number of samples from the beginning up to the beginning of the full prime signal:
        self._up_to_full_prime_samps = int(np.ceil(scaled_pw + scaled_ipi + scaled_b_prime))
        # Number of samples from the beginning through the end of the full prime signal:
        self._thru_full_prime_samps = int(np.ceil(sig_length - scaled_b_prime))
        self._total_sig_samps = int(np.ceil(sig_length))
        
        
    # Shape left out of general constructor because if they give a shape, you want it to call the
    # constructor for the more specific signal type
    def __init__(self, pulse_width, ratio, interpulse_interval, freq, full_sig_width = 0, deci_round = 10, n = 0):
        
        self._L_LARGE = 1
        
        self._pw = pulse_width
        self._ratio = ratio
        self._ipi = interpulse_interval
        self._freq = freq
        self._fsw = full_sig_width
        self._deci_round = deci_round

        # Correct n if it was left unspecified
        MIN_SAMPS_PER_SEG = 4
    
        if n == 0:
            smallest = pulse_width
            if interpulse_interval > 0:
                smallest = min(smallest, interpulse_interval)
            if full_sig_width > 0:
                smallest = min(smallest, full_sig_width)
                if pulse_width - full_sig_width > 0:
                    smallest = min(smallest, (pulse_width - full_sig_width)/2)
        
            self._n = int(np.ceil(MIN_SAMPS_PER_SEG/(smallest * freq))) + 1
        else:
            self._n = n
        
        
        
        # Round l_small here, unlike in the make_signal calculations, because
        # they are not used to calculate anything further.
        self._l_small = round(-1 * self._L_LARGE / ratio, deci_round)
        
        self._scale = self._freq * self._n
        
        self._set_bounds()
    
    
        
    def assertions(self, time_stamps):
        '''
        Each child class must implement this function.
        When called, this function checks to see that the values surrounding
        each segment joint in the signal falls in the range it's supposed to fall into.
        '''
        assert len(time_stamps) == self._n
        assert time_stamps[0] == 0
        assert np.isclose(time_stamps[len(time_stamps) - 1], 1/self._freq - (1/self._freq)/self._n)
    
    

class T_Ramp_Signal(T_Signal):
    
    def __init__(self, pulse_width, ratio, interpulse_interval, freq, full_sig_width = 0, deci_round = 10, n = 0):
        
        super().__init__(pulse_width, ratio, interpulse_interval, freq, full_sig_width, deci_round, n)
        self._shape = "ramp"
        
        
    def assertions(self):
        
        time_stamps, signal = ms(self._pw, self._ratio, self._ipi, self._freq, self._shape, self._fsw, self._deci_round, self._n, save_img = False)
        super().assertions(time_stamps)
    
        # Test the area around element zero
        print("zero")
        assert 0 == signal[0]
        assert 0 < signal[1] and signal[1] < self._L_LARGE

        # Test the area around the end of the first build-up ramp
        print("end of first b", self._b1_samps)
        assert 0 < signal[self._b1_samps - 1] and signal[self._b1_samps - 1]  < self._L_LARGE
        assert signal[self._b1_samps] == self._L_LARGE
        assert signal[self._b1_samps + 1] == self._L_LARGE

        # Test the area around the end of full_sig_width
        print("end of fsw", self._b_fsw_samps)
        assert signal[self._b_fsw_samps - 1] == self._L_LARGE
        assert 0 < signal[self._b_fsw_samps] and signal[self._b_fsw_samps] <= self._L_LARGE 
        assert signal[self._b_fsw_samps] > signal[self._b_fsw_samps + 1] and signal[self._b_fsw_samps + 1] >= 0

        # Test the area around the end of the pulse_width
        print("end of pw", self._pw_samps)
        assert self._L_LARGE > signal[self._pw_samps - 1] and signal[self._pw_samps - 1] >= 0
        if self._ipi != 0:
            assert signal[self._pw_samps] == 0
            assert signal[self._pw_samps + 1] == 0
        else:
            assert 0 >= signal[self._pw_samps] and signal[self._pw_samps] > self._l_small
            assert signal[self._pw_samps] > signal[self._pw_samps + 1] and signal[self._pw_samps + 1] >= self._l_small

        # Test the area around the end of the ipi
        print("end of ipi")
        if self._ipi != 0:
            assert signal[self._thru_ipi_samps - 1] == 0
        else:
            assert self._L_LARGE > signal[self._thru_ipi_samps - 1] and signal[self._thru_ipi_samps - 1] >= 0
        assert 0 >= signal[self._thru_ipi_samps] and signal[self._thru_ipi_samps] > self._l_small
        assert signal[self._thru_ipi_samps] > signal[self._thru_ipi_samps + 1] and signal[self._thru_ipi_samps + 1] >= self._l_small
        
        # Test the area around the end of the first build-up prime ramp
        print("end of first b prime", self._up_to_full_prime_samps)
        assert 0 > signal[self._up_to_full_prime_samps - 1] and signal[self._up_to_full_prime_samps - 1]  > self._l_small
        assert signal[self._up_to_full_prime_samps] == self._l_small
        assert signal[self._up_to_full_prime_samps + 1] == self._l_small

        # Test the area around the end of full_sig_width prime
        print("end of fsw prime", self._thru_full_prime_samps)
        assert signal[self._thru_full_prime_samps - 1] == self._l_small
        assert self._l_small <= signal[self._thru_full_prime_samps] and signal[self._thru_full_prime_samps] < 0
        assert signal[self._thru_full_prime_samps] < signal[self._thru_full_prime_samps + 1] and signal[self._thru_full_prime_samps + 1] <= 0

        # Test the area around the end of the pulse_width_prime
        print("end of pw prime", self._total_sig_samps)
        assert self._l_small < signal[self._total_sig_samps - 1] and signal[self._total_sig_samps - 1] <= 0
        if self._total_sig_samps < self._n:
            assert signal[self._total_sig_samps] == 0
            # Check that index isn't out of bounds
            if self._total_sig_samps < self._n - 1:
                assert signal[self._total_sig_samps + 1] == 0

        # Test the area around the end of the signal
        print("end of signal", len(signal) - 1)
        assert self._l_small < signal[len(signal) - 1] or signal[len(signal) - 1] == 0
        print('\n')

        

        
class T_Rect_Signal(T_Signal):
    
    # Rectangular signals need to override _set_bounds becuase they
    # don't have a build-up ramp to use for boundary calculations
    def _set_bounds(self):

        # scale_ variables represent the continuous number of samples
        # that should be allocated to each section
        # Scales are rounded for float rounding errors

        scaled_pw = round(self._pw * self._scale, self._deci_round)
        scaled_ipi = round(self._ipi * self._scale, self._deci_round)
        scaled_pw_prime = round(scaled_pw * self._ratio, self._deci_round)

        sig_length = scaled_pw + scaled_ipi + scaled_pw_prime


        # Number of samples from the beginning through the pulse width:
        self._pw_samps = int(np.ceil(scaled_pw))
        # Number of samples from the beginning through the ipi:
        self._thru_ipi_samps = int(np.ceil(scaled_pw + scaled_ipi))
        # Number of samples from the beginning through the end of the full prime signal:
        self._thru_pw_prime_samps = int(np.ceil(scaled_pw + scaled_ipi + scaled_pw_prime))
        self._total_sig_samps = int(np.ceil(sig_length))
        
        
    
    def __init__(self, pulse_width, ratio, interpulse_interval, freq, deci_round = 10, n = 0):
        
        super().__init__(pulse_width, ratio, interpulse_interval, freq, deci_round = deci_round, n = n)
        self._shape = "rectangular"
        
    
    def assertions(self):
        
        time_stamps, signal = ms(self._pw, self._ratio, self._ipi, self._freq, self._shape, self._fsw, self._deci_round, self._n, save_img = False)
        super().assertions(time_stamps)
        
        # Test the area around element zero
        print("zero")
        assert signal[0] == 0
        assert signal[1] == self._L_LARGE    

        # Test the area around the end of pw
        print("end of pw", self._pw_samps, self._pw)
        assert signal[self._pw_samps - 2] == self._L_LARGE
        # Different expectations depending on whether the rectangular rise/fall
        # lands on 0 or not
        if (self._pw * self._freq * self._n) % 1 == 0:
            assert signal[self._pw_samps - 1] == 0
            if self._ipi != 0:
                assert signal[self._pw_samps] == 0
                assert signal[self._pw_samps + 1] == 0
            else:
                assert signal[self._pw_samps] == self._l_small
                assert signal[self._pw_samps + 1] == self._l_small
        else:
            assert signal[self._pw_samps - 1] == self._L_LARGE
            if self._ipi != 0:
                assert signal[self._pw_samps] == 0
                assert signal[self._pw_samps + 1] == 0
            else:
                assert signal[self._pw_samps] == self._l_small
                assert signal[self._pw_samps + 1] == self._l_small

        # Test the area around the end of the ipi
        print("end of ipi", self._thru_ipi_samps)
        if self._ipi != 0:
            assert signal[self._thru_ipi_samps - 2] == 0
            assert signal[self._thru_ipi_samps - 1] == 0
            assert signal[self._thru_ipi_samps] == self._l_small or signal[self._thru_ipi_samps] == 0
        else:
            assert signal[self._thru_ipi_samps - 2] == self._L_LARGE
            if ((self._pw + self._ipi) * self._freq * self._n) % 1 == 0:
                assert signal[self._thru_ipi_samps - 1] == 0
            else:
                assert signal[self._thru_ipi_samps - 1] == self._L_LARGE
        assert signal[self._thru_ipi_samps] == self._l_small
        assert signal[self._thru_ipi_samps + 1] == self._l_small

        # Test the area around the end of pw prime
        print("end of pw prime")
        assert signal[self._thru_pw_prime_samps - 2] == self._l_small
        # Different expectations depending on whether the rectangular rise/fall
        # lands on 0 or not
        if ((self._pw * (1 + self._ratio) + self._ipi) * self._freq * self._n) % 1 == 0:
            assert signal[self._thru_pw_prime_samps - 1] == 0
            if self._thru_pw_prime_samps < self._total_sig_samps:
                assert signal[self._thru_pw_prime_samps] == 0
        else:
            assert signal[self._thru_pw_prime_samps - 1] == self._l_small
            if self._thru_pw_prime_samps < self._total_sig_samps:
                assert signal[self._thru_pw_prime_samps] == 0
        if self._thru_pw_prime_samps < self._total_sig_samps - 1:
            assert signal[self._thru_pw_prime_samps_pw_samps + 1] == 0

        # Test the area around the end of the pulse_width_prime
        assert self._total_sig_samps == self._thru_pw_prime_samps
        
        # Check that signal[total_sig_samps + 1] isn't out of bounds
        if self._total_sig_samps < self._n - 1:
            assert signal[self._total_sig_samps + 1] == 0

        # Test the area around the end of the signal
        print("end of signal", len(signal) - 1)
        assert signal[len(signal) - 1] == self._l_small or signal[len(signal) - 1] == 0
        print('\n')
       
   


class T_Tri_Signal(T_Signal):
    
    def __init__(self, pulse_width, ratio, interpulse_interval, freq, deci_round = 10, n = 0):
        
        super().__init__(pulse_width, ratio, interpulse_interval, freq, deci_round = deci_round, n = n)
        self._shape = "triangular"
    
    
    def assertions(self):
        
        time_stamps, signal = ms(self._pw, self._ratio, self._ipi, self._freq, self._shape, self._fsw, self._deci_round, self._n, save_img = False)
        super().assertions(time_stamps)
        
        # Test the area around element zero
        print("zero")
        assert 0 == signal[0]
        assert 0 < signal[1]

        # Test the area around the end of the first build-up ramp
        print("end of first b", self._b1_samps)
        assert 0 < signal[self._b1_samps - 1] and signal[self._b1_samps - 1]  < self._L_LARGE
        assert self._L_LARGE >= signal[self._b1_samps] and signal[self._b1_samps] >0
        assert signal[self._b1_samps] >= signal[self._b1_samps + 1]

        # Test the area around the end of full_sig_width
        assert self._b1_samps == self._b_fsw_samps

        # Test the area around the end of the pulse_width
        print("end of pw", self._pw_samps)
        assert self._L_LARGE > signal[self._pw_samps - 1] and signal[self._pw_samps - 1] >= 0
        if self._ipi != 0:
            assert 0 >= signal[self._pw_samps] and signal[self._pw_samps] > self._l_small
            assert 0 == signal[self._pw_samps + 1]
        else:
            assert self._L_LARGE > signal[self._pw_samps] and signal[self._pw_samps] > self._l_small
            assert 0 > signal[self._pw_samps + 1] and signal[self._pw_samps + 1] > self._l_small

        # Test the area around the end of the ipi
        print("end of ipi", self._thru_ipi_samps)
        if self._ipi != 0:
            assert signal[self._thru_ipi_samps - 1] == 0
        else:
            assert self._L_LARGE > signal[self._thru_ipi_samps - 1] and signal[self._thru_ipi_samps - 1] >= 0
        assert 0 >= signal[self._thru_ipi_samps] and signal[self._thru_ipi_samps] > self._l_small
        assert signal[self._thru_ipi_samps] > signal[self._thru_ipi_samps + 1] and signal[self._thru_ipi_samps + 1] >= self._l_small

        # Test the area around the end of the first build-up prime ramp
        print("end of second b", self._up_to_full_prime_samps)
        assert 0 >= signal[self._up_to_full_prime_samps - 1] and signal[self._up_to_full_prime_samps - 1] > self._l_small
        assert self._l_small <= signal[self._up_to_full_prime_samps] and signal[self._up_to_full_prime_samps] < 0
        assert signal[self._up_to_full_prime_samps] <= signal[self._up_to_full_prime_samps + 1] and signal[self._up_to_full_prime_samps + 1] <= 0

        # Test the area around the end of full_sig_width prime
        print("end of fsw prime", self._thru_full_prime_samps)
        assert self._thru_full_prime_samps == self._up_to_full_prime_samps

        # Test the area around the end of the pulse_width_prime
        print("end of pw prime", self._total_sig_samps)
        assert self._l_small <= signal[self._total_sig_samps - 1] and signal[self._total_sig_samps - 1] <= 0
        if self._total_sig_samps < self._n:
            assert signal[self._total_sig_samps] == 0
            if self._total_sig_samps < self._n - 1:
                assert signal[self._total_sig_samps + 1] == 0

        # Test the area around the end of the signal
        print("end of signal", len(signal) - 1)
        assert self._l_small < signal[len(signal) - 1] or signal[len(signal) - 1] == 0
        print('\n')
    
    

def gen_test_signal(pulse_width, ratio, interpulse_interval, freq, shape, full_sig_width = 0, deci_round = 10, n = 0):
    '''
    This function returns a T_Signal upon being passed the necessary parameters.
    It is used to keep shape_test generic among signal shapes.
    '''
    
    if shape == "ramp":
        return T_Ramp_Signal(pulse_width, ratio, interpulse_interval, freq, full_sig_width = full_sig_width, deci_round = deci_round, n = n)
    elif shape == "rectangular":
        return T_Rect_Signal(pulse_width, ratio, interpulse_interval, freq, deci_round = deci_round, n = n)
    elif shape == "triangular":
        return T_Tri_Signal(pulse_width, ratio, interpulse_interval, freq, deci_round = deci_round, n = n)
    else:
        raise Excpetion("Please select a test signal with a valid shape")


class T_Input():
    '''
    This class was written to make test inputs more flexible so that if we later
    decide to use different signal parameters, it will be easier to adopt the tests
    to match
    
    Note how the order of input for this class's constructor is NOT the same as the order
    for make_signal or the test signal classes.
    '''
    
    def __init__(self, pulse_width, ratio, interpulse_interval, freq, n, shape, full_sig_width = 0, deci_round = 10):
              
        
        self._pw = pulse_width
        self._ratio = ratio
        self._ipi = interpulse_interval
        self._freq = freq
        self._n = n
        self._shape = shape
        self._fsw = full_sig_width
        self._deci_round = deci_round
    
    def get_pw(self):
        return self._pw
    
    def get_ratio(self):
        return self._ratio
    
    def get_ipi(self):
        return self._ipi
    
    def get_freq(self):
        return self._freq
    
    def get_n(self):
        return self._n
    
    def get_shape(self):
        return self._shape
    
    def get_fsw(self):
        return self._fsw
    
    def get_deci_round(self):
        return self._deci_round
    
    def print_test(self):
        print(self.get_pw(), self.get_ratio(), self.get_ipi(), self.get_freq(), self.get_n(), self.get_shape(), self.get_fsw(), self.get_deci_round())

        
# Reads in the test cases for the given shape that aren't supposed to generate exceptions from the csv files
def get_shape_tests(shape):
    '''
    This function returns tests from the csv files that are meant to be a standard
    set of Test_Input objects that can be used to test each signal shape
    '''
    
    # Ramp shapes need an fsw greater than the sampling rate cutoff precision,
    # but rectangular and triangular shapes need an fsw of 0
    fsw = -1

    if shape == "ramp":
        fsw = 0.15
    else:
        fsw = 0

    # These are the standard shape test inputs meant to be applied to all signal shapes
    # Reading in the rows from the csv
    file = open("signal_tests/standard_signal_tests.csv")
    csvreader = csv.reader(file)
    rows = []
    for row in csvreader:
        rows.append(row)
    file.close()
    # Converting inputs from the rows in the csv into a list of T_Input objects for use in testing
    tests = []
    for row in rows[1:]:
        tests.append(T_Input(float(row[1]), float(row[2]), float(row[3]), float(row[4]), int(row[5]), shape, full_sig_width = fsw))
    
    # These are the specialized tests designed for each shape
    if shape == "ramp":
        file = open("signal_tests/standard_ramp_tests.csv")
    elif shape == "rectangular":
        file = open("signal_tests/standard_rect_tests.csv")   
    elif shape == "triangular":
        file = open("signal_tests/standard_tri_tests.csv")
    # Reading in the rows from the csv
    csvreader = csv.reader(file)
    rows = []
    for row in csvreader:
        rows.append(row)
    file.close()
    # Converting inputs from the rows in the csv into a list of T_Input objects for use in testing
    for row in rows[1:]:
        tests.append(T_Input(float(row[1]), float(row[2]), float(row[3]), float(row[4]), int(row[5]), shape, full_sig_width = float(row[6])))
        
    return tests
    

# Gets and runs tests  for the given shape that aren't supposed to generate exceptions
def shape_test(shape):
    '''
    This function is passed a signal shape to run tests on and
    runs the tests on signals of that shape.
    '''
    
    # get_tests generates an array of
    # the standard Test_Input objects for the given signal shape
    tests = get_shape_tests(shape)
    
    # This line allocates memory for test_signal outside of the for
    # loop so that computational effort is not wasted on deallocating
    # and reallocating the space in the for loop
    test_signal = T_Signal(-1, -1, -1, -1, -1)
    
    
    print("--- BEGINNING OF STANDARD TESTS FOR ", shape, "---\n")
    
    
    # This for loop goes through the Test_Inputs for the given shape, generates a signal based on each
    # Test_Input, and then runs the tests on the signal
    for test in tests:
        
        test.print_test()
        
        test_signal = gen_test_signal(test.get_pw(), test.get_ratio(), test.get_ipi(), test.get_freq(), shape, test.get_fsw(), test.get_deci_round(), test.get_n())
        
        test_signal.assertions()
        
        
    print("--- END OF STANDARD TESTS FOR ", shape, "---\n")
    
    
# Reads in the test cases for the given shape that generate exceptions from the csv files
def get_exception_tests(shape_in):    
    
    # Ramp shapes need an fsw greater than the n cutoff precision,
    # but rectangular and triangular shapes need an fsw of 0
    fsw = -1

    if shape_in == "ramp":
        fsw = 0.15
    else:
        fsw = 0

    # These are the exception test inputs meant to be applied to all signal shapes
    # Reading in the rows from the csv
    file = open("signal_tests/exception_signal_tests.csv")
    csvreader = csv.reader(file)
    rows = []
    for row in csvreader:
        rows.append(row)
    file.close()
    # Converting inputs from the rows in the csv into a list of T_Input objects for use in testing
    tests = []
    for row in rows[1:]:
        shape = shape_in
        if row[6] != '':
            shape = row[6]
        print(row[5])
        tests.append(T_Input(float(row[1]), float(row[2]), float(row[3]), float(row[4]), int(row[5]), shape, deci_round = int(row[7])))
    
    if shape == "ramp":
        file = open("signal_tests/exception_ramp_tests.csv")
    elif shape == "rectangular":
        file = open("signal_tests/exception_rect_tests.csv")
    elif shape == "triangular":
        file = open("signal_tests/exception_tri_tests.csv")
    # Reading in the rows from the csv
    csvreader = csv.reader(file)
    rows = []
    for row in csvreader:
        rows.append(row)
    file.close()
    # Converting inputs from the rows in the csv into a list of T_Input objects for use in testing
    for row in rows[1:]:
        tests.append(T_Input(float(row[1]), float(row[2]), float(row[3]), float(row[4]), int(row[5]), shape, full_sig_width = float(row[6])))
    
    return tests
    
    
    
# Gets and runs exception tests for the given shape
def exception_test(shape):
    
    tests = get_exception_tests(shape)
    
    
    print("--- BEGINNING OF EXCEPTION TESTS FOR ", shape, "---\n")
    
    for test in tests:
        
        test.print_test()
        
        with pytest.raises(Exception):
            ms(test.get_pw(), test.get_ratio(), test.get_ipi(), test.get_freq(), test.get_n(), test.get_shape(), test.get_fsw(), test.get_deci_round(), save_img = False)
            
    print("--- END OF EXCEPTION TESTS FOR ", shape, "---\n")        
    
    
    
    
# Test functions called when pytest runs:
    
    
def test_ramp():
    
    shape_test("ramp")
    exception_test("ramp")
    
    
def test_rect():
    
    shape_test("rectangular")
    exception_test("rectangular")
    
    
def test_tri():
    
    shape_test("triangular")
    exception_test("triangular")
    