# ELAINE Tissue Stimulation
ELAINE Tissue Stimulation (ETS) is a Python library build upon [NGSolve](https://ngsolve.org/) 
to model electrical field in the biological tissues. This package is a 
descendant of [OSS-DBS](https://github.com/SFB-ELAINE/OSS-DBS/tree/For_Lead-DBS/OSS_platform)
which is built upon the old implementation of [FEniCS](https://fenicsproject.org/). 

# Docs
Read the docs [here](https://jppayonk.github.io/ELAINE-TS/). 

## Description
Soon. 

## Visuals
Soon. 

## Installation
Soon. 

## Usage
Soon. 

## Support
Soon. 

## Roadmap
Soon. 

## Contributing
We would like to stick to the following conventions:
- [Sphinx](https://www.sphinx-doc.org/en/master/)-friendly documentation
- [PEP8](https://peps.python.org/pep-0008/), as much as possible 
- [pytest](https://docs.pytest.org/en/6.2.x/contents.html)-based unit tests


## Authors and acknowledgment
Soon. 

## License
Soon. 
